class Admin::BaseController < Spree::BaseController
  ssl_required

  before_filter :authenticate_user!

  #load_and_authorize_resource

  helper :search
  helper 'admin/navigation'
  layout 'admin'

  def admin_products_page
    if session[:back_to_page].present?
      admin_products_path(:page => session[:back_to_page])
    else
      admin_products_path
    end
  end


  protected

  def render_js_for_destroy
    render :partial => "/admin/shared/destroy"
    flash.notice = nil
  end


  # Index request for JSON needs to pass a CSRF token in order to prevent JSON Hijacking
  def check_json_authenticity
    return unless request.format.js? or request.format.json?
    auth_token = params[request_forgery_protection_token]
    unless (auth_token and form_authenticity_token == auth_token.gsub(' ', '+'))
      raise(ActionController::InvalidAuthenticityToken)
    end
  end


end