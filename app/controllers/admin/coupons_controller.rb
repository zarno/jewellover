class Admin::CouponsController < Admin::BaseController


  def index
    @coupons = Coupon.all.paginate(:page => params[:page])
  end

  def new
    @coupon = Coupon.new
    @coupon.number = @coupon.generate_number
  end

  def create
    @coupon = Coupon.new(params[:coupon])

    respond_to do |format|
      if @coupon.save
        # Send mail from model after_create
        format.html {redirect_to admin_coupons_path, :notice => "Your coupon has been created successfully"}
      else
        format.html {render :new, :alert => "There was a problem creating your coupon, try again."}
      end
    end
  end

  def update

  end

  def destroy

  end

end