class Admin::ListRecipientsController < Admin::BaseController

  def index
    @list = List.find(params[:list_id])
    @list_recipients = ListRecipient.where(:list_id => params[:list_id])
  end

  def destroy
    list_recipient = ListRecipient.find(params[:id])

    respond_to do |format|
      if list_recipient.delete
        format.html {redirect_to admin_list_contacts_path(list_recipient.list), :notice => "Recipient has been removed from list" }
      else
        format.html{ redirect_to admin_list_contacts_path(list_recipient.list), :alert => "There was a problem removing contact from list. Try Again" }
      end
    end
  end

end