class Admin::ListsController < Admin::BaseController

  def index
    @lists = List.includes(:list_recipients).search(params[:search])
  end

  def show
    @list = List.includes(:list_recipients, :users, :recipients).find(params[:id])
  end

  def edit
    @list = List.find(params[:id])
  end

  def new
    @list = List.new
  end

  def create
    @list = List.new(params[:list])

    respond_to do |format|
      if @list.save
        format.html { redirect_to admin_lists_path, :notice => "Your list was created successfully." }
      else
        format.html { redirect_to admin_lists_path, :error => "There was a problem saving your list." }
      end
    end
  end

  def update
    @list = List.find(params[:id])

    respond_to do |format|
      if @list.update_attributes(params[:list])
        format.html { redirect_to admin_lists_path, :notice => "Your list was updated successfully." }
      else
        format.html { redirect_to admin_lists_path, :error => "There was a problem updating your list." }
      end
    end
  end

  def destroy
    @list = List.find(params[:id])
    @list.destroy

    respond_to do |format|
      format.html { redirect_to admin_lists_path, :notice => "List destroyed successfully." }
    end
  end

  def upload_csv
    list = List.find(params[:id])

    f = open(params[:list][:file])
    bs = []

    # Get row integer of Email column
    rows = CSV.parse(f, :headers => false) do |row|
      if col
        col = row.first.index("Email")
        bs << row
      end
    end

    render :text => bs.to_json
  end

  def download_csv
    list = List.find(params[:list_id])
    contacts = list.emails
    filename = "#{list.name.gsub(/\s/, "_").downcase}_contacts_#{Date.today.strftime('%d%b%y')}"
    csv_data = CSV.generate do |csv|
      contacts.each do |c|
        csv << c
      end
    end

    logger.info("#{csv_data.inspect}")

    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{filename}.csv"
  end

end
