class Admin::PostsController < Admin::BaseController

  def index
    @posts = Post.all.paginate(:page => params[:page])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(params[:post])

    respond_to do |format|
      if @post.save
        format.html { redirect_to admin_posts_path, :notice => "#{@post.title} was saved Successfully" }
      else
        format.html { render :new, :notice => "#{@post.title} was saved Successfully" }
      end

    end

  end

  def edit
    @post = Post.includes(:post_role).find(params[:id])
  end

  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes params[:post]
        format.html { redirect_to admin_posts_path, :notice => "#{@post.title} was updated successfully" }
      else
        format.html { render edit_admin_post_path(@post), :notice => "There was a problem updating this post" }
      end
    end

  end

  def destroy
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.destroy
        format.html { redirect_to admin_posts_path, :notice => "#{@post.title} has been deleted successfully" }
      else
        format.html { redirect_to admin_posts_path, :notice => "There was a problem deleting this post" }
      end
    end

  end

end