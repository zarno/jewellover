Admin::ProductsController.class_eval do

  # CSV handling...
  require "csv"
  require "open-uri"

  index.before :index_before

  def toggle_relist

    product = Product.find(params[:id])
    product.toggle! :relist

    render :nothing => true

  end

  def product_campaign

    product = Product.find(params[:id])
    message = params[:message]
    subject = params[:subject]

    # TODO Make dynamic when we move to sendgrid

    users = %w(zarne@scratch22.net zarne.dravitzki@gmail.com themargyles@gmail.com)

    file = open("https://s3.amazonaws.com/jewellover_backups/emails/latest_emails.csv")
    emails = []

    read = CSV.foreach(file) do |row|
      emails << row[0] #emails is first row on aweberwhen moved better solutions needed. Hopefully API
    end

    # remove "email" header in CSV
    emails.delete("Email")

    emails.each do |user|
      AdministratorMailer.delay(:queue => "Mailers").product_campaign(user, product, message, subject)
    end

    redirect_to :back, :notice => "#{product.name} has been emailed as todays daily deal."
  end

  def reset

    product = Product.find_by_permalink(params[:id])

    if product.images.empty? || product.taxons.empty?
      flash[:error] = "Product must have at least 1 image and 1 category!"
      redirect_to admin_products_page
    else

      # If product has a future start time set to start at 'available_on'
      if product.available_on
        product.product_ready_reset
      else
        product.product_hard_reset
      end

      flash.notice = "Thank You - Your product has been listed and will go live at #{product.available_on.to_formatted_s(:long)}!"
      redirect_to admin_products_page

    end

  end

  def collection
    return @collection if @collection.present?

    products = Product.includes([{:variants => [:images, {:option_values => :option_type}]}, :master, :images, :store]).order("id ASC")

    unless request.xhr?
      params[:search] ||= {}
      # Note: the MetaSearch scopes are on/off switches, so we need to select "not_deleted" explicitly if the switch is off
      if params[:search][:deleted_at_is_null].nil?
        params[:search][:deleted_at_is_null] = "1"
      end

      params[:search][:meta_sort] ||= "name.asc"
      @search = products.metasearch(params[:search])

      pagination_options = {:per_page => Spree::Config[:admin_products_per_page],
                            :page => params[:page]}

      @collection = @search.paginate(pagination_options)
    else

      @collection = products.where(["name LIKE ?", "%#{params[:q]}%"]).limit(params[:limit] || 10)

      tmp = end_of_association_chain.where(["variants.sku LIKE ?", "%#{params[:q]}%"])
      tmp = tmp.includes(:variants_including_master).limit(params[:limit] || 10)

      @collection.concat(tmp)

      @collection.uniq
    end
  end


  def index_before
    session[:set_time] = nil
    session[:back_to_page] = params[:page].present? ? params[:page] : nil
  end


end
