class Admin::StoresController < Admin::BaseController

  def index
    @stores = Store.all.paginate(:page => params[:page])
  end

  def edit
    @store = Store.find_by_permalink(params[:id])
  end

  def update
    @store = Store.find_by_permalink(params[:id])

    respond_to do |format|
      if @store.update_attributes(params[:store])
        format.html { redirect_to admin_stores_path, :notice => "Store has been saved successfully" }
      else
        format.html { render :edit, :error => "There was a problem saving the store" }
      end
    end
  end


  def toggle_website
    @store = Store.find(params[:id])
    @store.toggle! :show_website

    render :nothing => true
  end

  def toggle_facebook
    @store = Store.find(params[:id])
    @store.toggle! :show_facebook

    render :nothing => true
  end

end