Admin::UsersController.class_eval do

  before_filter :authenticate_user!

  def toggle_ban

    user = User.find(params[:id])
    user.toggle_ban if current_user.has_role? "admin"

    render :nothing => true

  end




end