class Api::ProductsController < Api::BaseController

  before_filter :authenticate_user!, :except => [:index, :show, :taxonomies]

  def index

    if params[:retailer_id]
      @products = Product.search_by_name_or_sku(params[:search], params[:retailer_id])
    else
      @products = Product.search_by_name_or_sku(params[:search])
    end

    respond_with @products
  end

  def show
    @product = Product.find(params[:id])
    respond_with @product
  end

  def create

    retailer = Retailer.find(params[:retailer_id])

    product = retailer.products.build(params[:product])

    unless product.nil?

      product.leveredge = true
      product.cost_price = params[:product][:reserve_price]
      product.sku = params[:product][:stock_id]
      product.meta_description = params[:product][:description]
      product.meta_keywords = params[:product][:name].split(" ").join(",")
      product.on_hand = 1
      product.idle
      product.relist = true

      set_taxons(product) unless product.categories.empty?
    else
      return logger.info("Logged product == #{product.inspect} ")
    end


    respond_to do |format|

      if product.save
        format.xml { render :status => 201, :xml => {:notice => "Product created Successfully", :product_id => product.id} }
      else
        format.xml { render :status => 404, :xml => {:error => "Product creation was unsuccessful at this time"} }
      end

    end

  end

  def update

    product = Product.find(params[:id])

    images = Array.new

    params.each do |key, value|
      images << key.to_s if key.to_s =~ /image/
    end

    if params[:product]
      set_taxons(product, params[:product][:categories])
    end

    if !images.empty?

      images.each do |image|
        photo = product.images.new()
        photo.attachment = params[image.to_sym]
        photo.save
      end

    end

    respond_to do |format|
      if  product.update_attributes(params[:product])
        format.xml { render :status => 201, :xml => {:notice => "Product Updated Successfully"} }
      else
        format.xml { render :status => 404, :xml => {:error => "Product Update was unsuccessful at this time"} }
      end
    end
  end

  def delete_image

    image = Image.find(params[:image_id])

    respond_to do |format|

      if image.destroy
        format.xml { render :status => 200, :xml => {:notice => "Image Deleted Successfully"} }
      else
        format.xml { render :status => 204, :xml => {:notice => "No Image to delete"} }
      end

    end

  end

  def taxonomies

    @taxonomies = Taxonomy.all

    respond_to do |format|
      format.xml { render :status => 200 }
    end

  end


  private


  def set_taxons(product, cats = nil)


    if cats
      taxons = cats.split(",")
      product.taxons.clear

      taxons.each do |taxon|
        product.taxons << Taxon.find_or_create_by_name(taxon)
        logger.info("Should have inserted taxon named ==== #{taxon}")
      end
    else
      taxons = product.categories.split(",")

      taxons.each do |taxon|
        product.taxons << Taxon.find_or_create_by_name(taxon)
      end
    end


  end


end
