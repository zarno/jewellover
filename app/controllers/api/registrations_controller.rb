class Api::RegistrationsController < UserRegistrationsController


  def create

    @retailer = build_resource(params)

    if resource.save

      resource.create_watchlist
      resource.assign_role("retailer")

      respond_to do |format|
        format.html { super }
        format.xml { render :status => 201, :xml => {:error => "Success"} }
      end


    else

      respond_to do |format|
        format.html { super }
        format.xml { render :status => 404, :xml => {:error => "Resource could not be saved at this time, check with administrator"} }
      end

    end

  end


end