class Api::RetailersController < Api::BaseController

  def index
    if params[:search]
      @retailers = Retailer.where("authentication_token = ?", params[:search])
    else
      @retailers = Retailer.all
    end
    respond_with @retailers
  end

  def show
    @retailer = Retailer.find(params[:id])
    respond_with @retailer
  end

end