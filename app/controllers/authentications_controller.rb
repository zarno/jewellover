class AuthenticationsController < Spree::BaseController

  # POST /authentications
  # POST /authentications.xml
  def create
    omniauth = request.env["omniauth.auth"]
    authentication = Authentication.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])

    logger.info("Hitme 1")
    #logger.info("AUth ==== #{authentication.inspect}")
    # render :text => omniauth.to_json and return

    if authentication
      logger.info("Hitme 2")
      flash[:notice] = "Signed in successfully."

      sign_in_and_redirect(:user, authentication.user)
    elsif current_user
      logger.info("Hitme 3")
      current_user.authentications.find_or_create_by_uid(:provider => omniauth['provider'], :uid => omniauth['uid'])
      flash[:notice] = "Authentication successful."
      redirect_to root_url
    else
      user = User.new
      logger.info("Hitme 4")

      user.apply_omniauth(omniauth)

      if user.save
        flash[:notice] = "Signed in successfully."

        sign_in_and_redirect(:user, user)
      else
        logger.info(user.errors)
        session[:omniauth] = omniauth

        flash[:error] = "Something went wrong with your authentication"
        redirect_to signup_path
      end

    end

  end

  def failure
    redirect_to signup_path, :alert => "There was a problem signing in."
  end

  private

  def after_sign_in_path_for(resource)
    my_dashboard_path
  end

end