class BidsController < Spree::BaseController

  before_filter :authenticate_user!

  def index

    #need to seperate bids into current live bids whether leading or not, won bids, and lost bids

    @bids = current_user.bids
    @current_bids = current_user.current_bids
    @won_bids = current_user.won_bids
    @lost_bids = current_user.lost_bids

  end

  def create
    params_amount = params[:bid][:amount].gsub(",", "")

    @product = Product.find(params[:product])
    @bid = Bid.new(:user_id => current_user.id, :bidbucket_id => @product.bidbucket.id, :amount => params_amount)

    respond_to do |format|

      if @product.bidbucket.bids.collect { |b| @bid.amount <= b.amount }.include?(true)
        # if current bid is less than earlier bid forget about adding it.
        format.html { redirect_to(@product, :alert => "Your offer must be higher than any previous offers, please try again") }

      elsif params[:bid][:amount].to_i > @product.current_price
        format.html { redirect_to(@product, :alert => "You cannot pay more than the current price for this product, click Buy Now to purchase at this current price!") }
      elsif params[:bid][:amount].to_i <= @product.cost_price
        format.html { redirect_to(@product, :alert => "Your offer must be greater than the end price!") }
      else
        if @bid.save


          @product.update_kill_time

          if @product.upcoming?
            @product.set_product_kill
          else
            @product.update_kill_job
            logger.info("BID SUCCESSFUL =============> #{@product.job}")
          end

          # Sort bids from highest to lowest then alert the losing offer of losing
          bids = @product.bids.sort { |x, y| y.amount <=> x.amount }
          user = bids[0].user # User who created this bid


          # Send email to bid user who just got beaten.
          if bids.count > 1
            loser = bids[1].user # Loser only exists when there are more than 1 Bids.
            OfferMailer.delay(:queue => 'mailers').offer_beaten(loser, @product)
          end

          OfferMailer.delay(:queue => 'mailers').offer_placed(user, @product)

          format.html { redirect_to(@product, :notice => 'Thank You - Your offer has been placed') }
          format.xml { render :xml => @product, :status => :created, :location => @product }
        else
          format.html { redirect_to(@product, :alert => "There was a problem placing your offer. Please refresh the page and try again") }
          format.xml { render :xml => @product.errors, :status => :unprocessable_entity }
        end

      end

    end

  end

  private

  def create_order
    @order = current_order(true)
  end


end
