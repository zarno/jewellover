CheckoutController.class_eval do

  protect_from_forgery :except => [:paypal_adaptive_payment_finish]
  before_filter :redirect_to_paypal_adaptive_payment_form_if_needed, :only => [:update]
  before_filter :get_address, :only => :edit

  def paypal_adaptive_payment_checkout
    load_order
    opts = all_opts(@order, params[:payment_method_id], 'checkout')
    opts.merge!(address_options(@order))

  rescue ActiveMerchant::ConnectionError => e
    gateway_error I18n.t(:unable_to_connect_to_gateway)
    redirect_to :back
  end

  # Updates the order and advances to the next state (when possible.)
  def update

    if params[:save_address] == "1"
      addy = params[:order][:bill_address_attributes]
      current_user.user_preference.update_attributes(
          :fname => addy[:firstname],
          :lname => addy[:lastname],
          :address1 => addy[:address1],
          :address2 => addy[:address2],
          :city => addy[:city],
          :state => State.find_by_id(addy[:state_id]).name,
          :postcode => addy[:zipcode],
          :country => Country.find_by_id(addy[:country_id]).name,
          :phone => addy[:phone]
      )
    end

    if @order.redemption? && params[:order].present?

      logger.info(" ======= >>>>> Got it  + #{object_params}")

      if params[:order][:coupon].present?
        coupon = Coupon.find_by_number(params[:order][:coupon])

        if coupon.present? && coupon.valid?
          coupon.update_attributes({
                                       :order_id => @order.id,
                                   })
          @order.adjustments.create({
                                        :label => "Coupon",
                                        :amount => (-coupon.value),
                                        :source => coupon,
                                        :source_id => coupon.id,
                                        :mandatory => false
                                    })
        end
      end


    end


    if @order.update_attributes(object_params)
      if @order.next
        state_callback(:after)
      else
        flash[:error] = I18n.t(:payment_processing_failed)
        redirect_to checkout_state_path(@order.state) and return
      end

      if @order.state == "complete" || @order.completed?
        flash[:notice] = I18n.t(:order_processed_successfully)
        flash[:commerce_tracking] = "nothing special"
        redirect_to completion_route
      else
        redirect_to checkout_state_path(@order.state)
      end

    else
      render :edit
    end
  end

  def paypal_adaptive_payment
    load_order
    opts = all_opts(@order, params[:payment_method_id], 'payment')
    opts.merge!(address_options(@order))
    gateway = paypal_adaptive_payment_gateway

    recipients = [
        {:email => opts[:retailer_email],
         :amount => opts[:order_amount],
         :invoice_id => opts[:order_id],
         :primary => true},
        {:email => JEWELLOVER_CONFIG['email'],
         :amount => opts[:amount_jewellover],
         :invoice_id => opts[:order_id],
         :primary => false}
    ]

    req = {
        :memo => opts[:description],
        :fees_payer => "PRIMARYRECEIVER",
        :receiver_list => recipients,
        :senderEmail => opts[:email],
        :return_url => opts[:return_url],
        :cancel_url => opts[:cancel_return_url],
        :action_type => "PAY", #'CREATE' for SetPaymentOptions - Business Name, Header Image, Item Name etc
        :currency_code => "AUD"
    }

    session[:ap_response] = gateway.setup_purchase(req)

    logger.info("===== Order --> #{@order.inspect}")
    logger.info("===== Recipients --> #{recipients.inspect}")
    logger.info("===== Ap Request --> #{req.inspect}")
    logger.info("===== Ap Response --> #{session[:ap_response].inspect}")

    unless (session[:ap_response].success?)
      gateway_error(session[:ap_response])
      redirect_to edit_order_checkout_url(@order, :state => "payment")
      return
    end


    redirect_to (gateway.redirect_url_for(session[:ap_response][:pay_key]))

  rescue ActiveMerchant::ConnectionError => e
    gateway_error I18n.t(:unable_to_connect_to_gateway)
    redirect_to :back
  end

  def paypal_adaptive_payment_confirm

    logger.info("======>>>>> paypal_adaptive_payment_confirm --- REQUEST.env ---->>> #{request.env.inspect}")

    logger.info("=======>>>> @payment_complete is set to #{@payment_complete}")

    load_order

    opts = {
        :token => session[:ap_response][:pay_key],
        :payer_id => session[:ap_response][:pay_key]
    }.merge all_opts(@order, params[:payment_method_id], 'payment')

    logger.info("session[:ap_response] =======>>>> #{session[:ap_response].inspect}")

    if session[:ap_response].success?

      logger.info("====> session[ap_response] is a success!!!")

      PaypalAdaptivePaymentAccount.create(
          :email => current_user.email,
          :payer_id => session[:ap_response][:pay_key],
          :payer_country => 'Country',
          :payer_status => session[:ap_response][:payment_exec_status])

      @order.special_instructions = session[:ap_response][:note]
      @order.save

      paypal_adaptive_payment_finish

    else
      gateway_error(session[:ap_response])

      logger.info("==== >>> Ap response did not succedd")

      if @order.complete?
        logger.info("==== >>> Order has already been complete, Damn paypal return buttons")
        redirect_to orders_path + "#complete", :notice => "Congratulations, your purchase was complete!"
      else
        logger.info("==== >>> Genuine good stuff")
        redirect_to edit_order_checkout_url(@order, :state => "payment")
      end

    end
  rescue ActiveMerchant::ConnectionError => e
    gateway_error I18n.t(:unable_to_connect_to_gateway)
    redirect_to edit_order_url(@order)
  end


  def paypal_adaptive_payment_finish


    #logger.info("======>>>>> paypal_adaptive_payment_finish --- REQUEST.env ---->>> #{request.env.inspect}")


    load_order

    opts = {
        :token => session[:ap_response][:pay_key],
        :payer_id => session[:ap_response][:pay_key]
    }.merge all_opts(@order, params[:payment_method_id], 'payment')

    product = @order.product
    gateway = paypal_adaptive_payment_gateway
    ap_details = gateway.details_for_payment(session[:ap_response])
    paypal_account = PaypalAdaptivePaymentAccount.find_by_payer_id(session[:ap_response][:pay_key])

    payment = @order.payments.create(
        :amount => @order.payment_total.to_f,
        :source => paypal_account,
        :source_type => 'PaypalAdaptivePaymentAccount',
        :payment_method_id => params[:payment_method_id],
        :response_code => session[:ap_response][:pay_key],
        :avs_response => session[:ap_response][:response_envelope][:correlation_id])

    payment.started_processing!

    logger.info("=======>>>> AP DETAILS --> #{ap_details.inspect}")

    if ap_details.success?

      payment.complete!

      until @order.state == "complete"
        if @order.next!
          @order.update!
        end
      end

      if @order.coupon.present?
        @order.coupon.update_attribute :used_at, Time.now
      end

      session[:order_id] = nil
      session[:ap_response] = nil
      @current_order = nil
      product.sold

      logger.info("========= >>>>>>> LOGGER --> #{product.inspect}")

      @payment_complete = true


      redirect_to orders_path + "#complete", :notice => "Congratulations, your purchase was complete!" and return

    else


      if @order.complete?
        logger.info("==== >>> Order has already been complete, Damn paypal return buttons")
        redirect_to orders_path + "#complete", :notice => "Congratulations, your purchase was complete!"
      else

        payment.fail!
        order_params = {}
        gateway_error(ap_details)

        redirect_to edit_order_checkout_url(@order, :state => "payment")
      end
    end
  rescue ActiveMerchant::ConnectionError => e
    gateway_error I18n.t(:unable_to_connect_to_gateway)
    redirect_to edit_order_url(@order)
  end

  # The rest in the "private" world pretty much is the same
  # as the express gem... sort of for consistency...
  private

  def record_log(payment, response)
    payment.log_entries.create(:details => response.to_yaml)
  end

  def redirect_to_paypal_adaptive_payment_form_if_needed
    return unless (params[:state] == "payment")
    return unless params[:order][:payments_attributes]
    if params[:order][:coupon_code]
      @order.update_attributes(object_params)
      @order.process_coupon_code
    end
    load_order
    payment_method = PaymentMethod.find(params[:order][:payments_attributes].first[:payment_method_id])
  end

  def fixed_opts
    if Spree::Config[:paypal_express_local_confirm].nil?
      user_action = "continue"
    else
      user_action = Spree::Config[:paypal_express_local_confirm] == "t" ? "continue" : "commit"
    end

    {:description => "Goods from #{Spree::Config[:site_name]}", # site details...

     #:page_style             => "foobar", # merchant account can set named config.rb
     #:headerImageUrl => "http://#{Spree::Config[:site_url]}#{Spree::Config[:logo]}",
     #:background_color => "ffffff", # must be hex only, six chars
     #:header_background_color => "ffffff",
     #:header_border_color => "ffffff",
     :allow_note => true,
     :locale => Spree::Config[:default_locale],
     :req_confirm_shipping => false, # for security, might make an option later
     :user_action => user_action

     # WARNING -- don't use :ship_discount, :insurance_offered, :insurance since
     # they've not been tested and may trigger some paypal bugs, eg not showing order
     # see http://www.pdncommunity.com/t5/PayPal-Developer-Blog/Displaying-Order-Details-in-Express-Checkout/bc-p/92902#C851
    }
  end

  def order_opts(order, payment_method, stage)
    items = order.line_items.map do |item|
      price = (item.price * 100).to_i # convert for gateway
      {:name => item.variant.product.name,
       :description => (item.variant.product.description[0..120] if item.variant.product.description),
       :sku => item.variant.sku,
       :quantity => item.quantity,
       :amount => price,
       :weight => item.variant.weight,
       :height => item.variant.height,
       :width => item.variant.width,
       :depth => item.variant.weight}
    end

    credits = order.adjustments.map do |credit|
      if credit.amount < 0.00
        {:name => credit.label,
         :description => credit.label,
         :sku => credit.id,
         :quantity => 1,
         :amount => (credit.amount*100).to_i}
      end
    end

    credits_total = 0
    credits.compact!
    if credits.present?
      items.concat credits
      credits_total = credits.map { |i| i[:amount] * i[:quantity] }.sum
    end

#:return_url => "http://" + request.host_with_port + "/orders/#{order.number}/checkout/paypal_adaptive_payments_confirm?payment_method_id=#{payment_method}",


    opts = {:return_url => paypal_adaptive_payment_confirm_order_checkout_url(order, :payment_method_id => payment_method),
            :cancel_return_url => "http://" + request.host_with_port + "/orders/#{order.number}/edit",
            :order_id => order.number,
            :custom => order.number,
            :items => items,
            :subtotal => ((order.item_total * 100) + credits_total).to_i,
            :tax => ((order.adjustments.map { |a| a.amount if (a.source_type == 'Order' && a.label == 'Tax') }.compact.sum) * 100).to_i,
            :shipping => ((order.adjustments.map { |a| a.amount if a.source_type == 'Shipment' }.compact.sum) * 100).to_i,
            :money => (order.total * 100).to_i}

    # add correct tax amount by subtracting subtotal and shipping otherwise tax = 0 -> need to check adjustments.map
    opts[:tax] = (order.total*100).to_i - opts.slice(:subtotal, :shipping).values.sum

    if stage == "checkout"
      opts[:handling] = 0
      opts[:callback_url] = "http://" + request.host_with_port + "/paypal_adaptive_payment_callbacks/#{order.number}"
      opts[:callback_timeout] = 3
    elsif stage == "payment"
      #hack to add float rounding difference in as handling fee - prevents PayPal from rejecting orders
      #because the integer totals are different from the float based total. This is temporary and will be
      #removed once Spree's currency values are persisted as integers (normally only 1c)
      opts[:handling] = (order.total*100).to_i - opts.slice(:subtotal, :tax, :shipping).values.sum
    end
    opts
  end

  def address_options(order)
    if payment_method.preferred_no_shipping
      {:no_shipping => true}
    else
      {
          :no_shipping => false,
          :address_override => true,
          :address => {
              :name => "#{order.ship_address.firstname} #{order.ship_address.lastname}",
              :address1 => order.ship_address.address1,
              :address2 => order.ship_address.address2,
              :city => order.ship_address.city,
              :state => order.ship_address.state.nil? ? order.ship_address.state_name.to_s : order.ship_address.state.abbr,
              :country => order.ship_address.country.iso,
              :zip => order.ship_address.zipcode,
              :phone => order.ship_address.phone
          }
      }
    end
  end

  # hook to override paypal site options
  def paypal_site_opts
    {:currencyCode => payment_method.preferred_currency}
  end

  def split_jewellover_payment(order_total)
    a = JEWELLOVER_CONFIG['percent']
    b = a.to_f / 100
    c = order_total * b
    c.round(2)
  end

  def all_opts(order, payment_method, stage=nil)
    opts = fixed_opts.merge(order_opts(order, payment_method, stage)).merge(paypal_site_opts)

    if stage == "payment"
      opts.merge! flat_rate_shipping_and_handling_options(order, stage)
    end

    # Order Details
    opts[:retailer_email] = order.retailer.retailer_detail.pp_email
    opts[:order_amount] = order.total
    opts[:amount_jewellover] = split_jewellover_payment(order.total)

    opts
  end

  # hook to allow applications to load in their own shipping and handling costs
  def flat_rate_shipping_and_handling_options(order, stage)
    # max_fallback = 0.0
    # shipping_options = ShippingMethod.all.map do |shipping_method|
    #       max_fallback = shipping_method.fallback_amount if shipping_method.fallback_amount > max_fallback
    #           { :name       => "#{shipping_method.id}",
    #             :label       => "#{shipping_method.name} - #{shipping_method.zone.name}",
    #             :amount      => (shipping_method.fallback_amount*100) + 1,
    #             :default     => shipping_method.is_default }
    #         end
    #
    #
    # default_shipping_method = ShippingMethod.find(:first, :conditions => {:is_default => true})
    #
    # opts = { :shipping_options  => shipping_options,
    #          :max_amount  => (order.total + max_fallback)*100
    #        }
    #
    # opts[:shipping] = (default_shipping_method.nil? ? 0 : default_shipping_method.fallback_amount) if stage == "checkout"
    #
    # opts
    {}
  end

  def gateway_error(response)
    logger.error(response.error)
    # "There was an error, Please try again... if you have no luck please contact us"
    flash[:error] = response.error
  end

  def get_address
    @user_addy = current_user.user_preference ||= nil
  end

  def payment_method
    PaymentMethod.find(params[:payment_method_id])
  end

  def paypal_adaptive_payment_gateway
    payment_method.provider
  end
end