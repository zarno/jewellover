class ContactsController < Spree::BaseController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    respond_to do |format|
      if @contact.valid? && @contact.save
        ContactMailer.delay(:queue => 'mailers').message_email(@contact)
        ContactMailer.delay(:queue => 'mailers').admin_email(@contact)
        format.html { redirect_to(contact_path, :notice => t("message_sent")) }
      else
        format.html { render :action => "new", :alert => "There was a problem sending your message, please try again." }
      end
    end
  end

  def store
    @store = Store.find(params[:store_id])
    @message = Contact.new(params[:contact])

    logger.info("======== >>>>> CONTACT STORE == > #{@store.inspect}")

    if @message.valid? && @message.save
      RetailerMailer.delay(:queue => "mailers").contact_message(@message, @store)
      redirect_to :back, :notice => "Thank you, your message has been sent."
    else
      redirect_to :back, :alert => "There was a problem sending your message, please try again."
    end

  end

end