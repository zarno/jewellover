class DashboardController < Spree::BaseController

  before_filter :authenticate_user!, :except => :server_time

  def index
    @user = current_user
    @wproducts = @user.watchlist.products.select{|p| p.live? || p.upcoming? }.first(10)
    @bids = @user.bids.group_by { |b| b.bidbucket }
    @orders = @user.orders
    @products = Product.where_live
    @user_prefs = @user.user_preference
  end


end