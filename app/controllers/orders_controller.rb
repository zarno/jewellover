class OrdersController < Spree::BaseController

  before_filter :authenticate_user!
  before_filter :check_for_orders, :only => :populate

  helper :products

  def index
    @orders = Order.select { |o| o.user_id == current_user.id }
  end

  def show
    @order = Order.find_by_number(params[:id])
    @product = @order.product
    @store = @product.store
  end

  def update

    @order = current_order

    if @order.update_attributes(params[:order])
      @order.line_items = @order.line_items.select { |li| li.quantity > 0 }
      redirect_to cart_path
    else
      render :edit
    end

  end

  def edit
    @order = current_order
  end

  # Adds a new item to the order (creating a new order if none already exists)
  #
  # Parameters can be passed using the following possible parameter configurations:
  #
  # * Single variant/quantity pairing
  # +:variants => {variant_id => quantity}+
  #
  # * Multiple products at once
  # +:products => {product_id => variant_id, product_id => variant_id}, :quantity => quantity +
  # +:products => {product_id => variant_id, product_id => variant_id}}, :quantity => {variant_id => quantity, variant_id => quantity}+
  def populate

    session[:order_id] = nil
    @current_order = nil

    @order = current_order(true)


    params[:variants].each do |variant_id, quantity|

      quantity = 1
      @order.add_variant(Variant.find(variant_id), quantity) if quantity > 0

    end if params[:variants]


    #update product state to cart
    @order.product.cart

    # we want to delete the Kill_product Delayed::Job because it has just been SOLD!... well 'carted' anyway
    # If they push buy now in the final seconds this could still run the Job(IE Relist or expire) if it doesnt get called fast enough..
    # Really in theory theres no way to control the JOb if its set to run within the time it takes to
    # reach this point. All will come down to speed of the connection/site...
    @order.product.delete_kill_job

    #set owner details for order
    @order.update_attributes(:retailer_id => @order.product.retailer_id)


    redirect_to cart_path

  end

  def empty
    if @order = current_order
      #clear session order id because each order will be New when buy now selected.
      #NOTE nil is true and false is false in current_order module
      session[:order_id] = nil
      @order.line_items.destroy_all
      @order.product.cart unless @order.product.sold
      @order.delete
    end
    redirect_to cart_path
  end

  def check_for_orders

    existing_orders = Order.joins(:line_items).where("line_items.variant_id = ?", params[:variants].first[0])

    unless existing_orders.empty? ||
        %w(canceled refunded).include?(existing_orders.last.state)
      redirect_to session[:user_return_to], :alert => "This product has been sold!"
    end

  end


  def accurate_title
    I18n.t(:shopping_cart)
  end

end