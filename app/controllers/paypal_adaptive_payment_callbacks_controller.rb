class PaypalAdaptivePaymentCallbacksController < Spree::BaseController
  include ActiveMerchant::Billing::Integrations

  skip_before_filter :verify_authenticity_token
  protect_from_forgery :except => [:notify]

  def notify
    logger.info("=>>>> Paypal NOTIFICATION parameters!!!! #{params.inspect}")

    retrieve_details #need to retreive details first to ensure ActiveMerchant gets configured correctly.

    @notification = PaypalAdaptivePayment::Notification.new(request.raw_post)

    logger.info("=>>>> First Paypal NOTIFICATION COMPLETE!!!! #{@notification.inspect}")

    # we only care about eChecks (for now?)
    if @notification.params["action_type"] == "PAY" && @notification.acknowledge
      @payment.log_entries.create(:details => @notification.to_yaml)

      @payment = @order.payments.create(
                                 :amount => '111.11',
                                 :source => paypal_account,
                                 :source_type => 'PaypalAdaptivePaymentAccount',
                                 :payment_method_id => params[:payment_method_id],
                                 :response_code => 'ack',
                                 :avs_response => 'avs_response')

      @payment.started_processing!

      logger.info("=>>>> Paypal NOTIFICATION COMPLETE!!!! #{@notification.inspect}")
      logger.info("=>>>> Paypal order before complete!!!! #{@order.inspect}")
      logger.info("=>>>> Paypal payment before complete!!!! #{@payment.inspect}")
      case @notification.params["status"]
      when "ERROR"
        logger.info("=>>>> Paypal NOTIFICATION ERROR!!!! #{@notification.inspect}")
        @payment.fail!
      when "COMPLETED"
        @payment.complete!
        @order.state = 'complete'
        logger.info("=>>>> Paypal order after complete!!!! #{@order.inspect}")
        logger.info("=>>>> Paypal payment after complete!!!! #{@payment.inspect}")
        logger.info("=>>>> Paypal Order payment after complete!!!! #{@order.payments.inspect}")
      end
    else
      logger.info("=>>>> Paypal NOTIFICATION NOT ACKNOWLDGED!!!! #{@notification.inspect}")
    end
    render :nothing => true
  end

  private
  def retrieve_order
    @order = Order.find_by_number(params['transaction']['0']['.invoiceId'])
  end
end
