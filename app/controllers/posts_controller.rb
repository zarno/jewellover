class PostsController < Spree::BaseController

  def testimonials
    @posts = Post.testimonials
  end

  def whats_new
    @posts = Post.whats_new
  end

end