ProductsController.class_eval do

  before_filter :set_return_to, :only => :show

  def index
    @title = "JewelLover - Products"
    @meta_keywords = "jewelry, online jewellery, online jewelry, discount jewellery, discount jewelry, jewellery auction, jewelry auction"
    @meta_description = "Discount jewellery where prices drop til you shop. We sell diamonds, silver jewellery, gold, pendants, earrings, necklaces,watches, bracelets, charms and beads and other jewellery online from jewellers across Australia. The JewelLover website sells jewellery from retail and online jewellers via a unique discount jewellery auction process"

    if params[:store_id]
      @store = Store.find_by_permalink(params[:store_id])
      @search = @store.products.where_online
      @context = "in #{@store.name}"
    elsif params[:context] == "upcoming"
      @search = Product.where_upcoming
    else
      @search = Product.where_online
    end

    @search = @search.search(params[:search])

    if params[:sort_by]
      @products = @search.relation.instance_eval(params[:sort_by])
    else
      @products = @search.all
    end

    @products = @products.paginate(:page => params[:page], :per_page => get_per_page)


    if params[:context]
      case params[:context]
        when "upcoming"
          redirect_to upcoming_path(:search => params[:search], :commit => "Search")
        else
          render :index
      end
    end


  end

# PAGES
  def homepage
    @title = "Online jewellery clearance deals from your favourite jewellers at Jewellover"
    @meta_keywords = "jewellery, jewelry, jewellers, diamonds, rings, watches, diamond jewellery, silver jewellery, gold jewellery, earrings, bracelets, bangles, beads,  "
    @meta_description = "Online jewellery from Australian Jewellers including Diamonds Rings and Watches - where prices til drop you shop, only at Jewellover."

    # AIM: List unique stores on homepage with highest percentage
    # All live products ordered by Highest Percentage off
    products = Product.where_live.sort { |x, y| y.drop_count <=> x.drop_count }
    unique_products = products.uniq { |p| p.store }

    #If total products with unique stores is less than 10
    if unique_products.count < 10
      p = unique_products + products # additional products from previous product index
      @products = p.uniq.take(10) #uniq and only 10 total
    else
      @products = unique_products.take(10)
    end

    respond_to do |format|
      if request.path == "/v2"
        format.html { render :template => 'products/homepage2' }
      else
        format.html { render :template => 'products/homepage'}
      end
    end


  end

  def categories
    @title = "Jewellery Categories"
    @meta_keywords = "rings, diamonds, beads, fashion, gold, silver, necklace, watch, mens, womans"
    @meta_description = "Find your favourite Rings, Watches, Necklaces, Beads in Gold, Silver, Pink or Sapphire"

    @stores = Store.select { |r| r.products.count > 0 }.sample(10)
  end

  def upcoming
    @title = "Upcoming Jewellery"
    @meta_keywords = ""
    @meta_description = "Upcoming deals waiting to start dropping in price, watch your favourite jewellery items drop in price."


    @search = Product.where_upcoming.search(params[:search])

    if params[:sort_by]
      @products = @search.relation.instance_eval(params[:sort_by])
    else
      @products = @search.all
    end

    @products = @products.paginate(:page => params[:page], :per_page => get_per_page)
    @context = "in Upcoming Deals"

  end

  def latest
    @products = Product.where_sold.paginate(:page => params[:page], :per_page => get_per_page)
  end


  def show

    @product = Product.with_show_includes.find_by_permalink(params[:id])

    @bids = @product.bidbucket.bids.order("created_at DESC")
    @bid = Bid.new
    @store = @product.store
    @more_products = get_more_product


    respond_to do |format|
      if %w(expired idle).include? @product.state
        format.html { redirect_to root_path, :alert => "This item has expired." }
      else
        format.html
      end
    end

  end

  def view_style
    cookies[:view_style] = params[:view_style] unless params[:view_style].empty?
    cookies[:view_amount] = params[:view_amount]

    redirect_to :back
  end


  private


  def get_more_product

    # Next product is stores product if within store realm, or display next item in category
    if params[:store_id].present?
      products = @store.products.where_live.sample(5)
    else
      products = Product.where_live.from_category(@product.taxons.first.name).sample(5)
    end

    products

  end

  def set_return_to
    session[:user_return_to] = product_path(@product)
  end

end


