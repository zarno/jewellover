class Retailer::BaseController < Spree::BaseController
  ssl_required

  before_filter :authenticate_user!
  before_filter :load_retailer


  authorize_resource

  helper :search
  helper 'retailer/navigation'
  layout 'retailer'


  def retailer_products_page
    if session[:back_to_page].present?
      retailer_products_path(:page => session[:back_to_page])
    else
      retailer_products_path
    end
  end

  protected


  def render_js_for_destroy
    render :partial => "/retailer/shared/destroy"
    flash.notice = nil
  end


  # Index request for JSON needs to pass a CSRF token in order to prevent JSON Hijacking
  def check_json_authenticity
    return unless request.format.js? or request.format.json?
    auth_token = params[request_forgery_protection_token]
    unless (auth_token and form_authenticity_token == auth_token.gsub(' ', '+'))
      raise(ActionController::InvalidAuthenticityToken)
    end
  end

  private


  def load_retailer
    @retailer = Retailer.find(current_user)
    @stores = @retailer.stores
  end


end
