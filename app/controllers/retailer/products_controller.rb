class Retailer::ProductsController < Retailer::BaseController

  resource_controller
  before_filter :check_json_authenticity, :only => :index
  before_filter :load_data, :except => :index


  index.before :index_before
  index.response do |wants|
    wants.html { render :action => :index }
    wants.json { render :json => json_data }
    wants.xml { render :xml => collection }
  end


  update.before :update_before
  update.response do |wants|
    wants.html { redirect_to next_url }
  end


  def refund
    product = Product.find_by_permalink(params[:id])

    product.order.refund

    RetailerMailer.delay(:queue => 'mailers').order_refunded(product.order)
    UserMailer.delay(:queue => 'mailers').order_refunded(product.order)


    redirect_to retailer_products_path, :notice => "Order ##{product.order.number} has been refunded!"

  end

  def archive
    product = Product.find_by_permalink(params[:id])

    product.archive

    if product.deleted?
      redirect_to retailer_products_page, :notice => "#{product.name} has been archived!"
    else
      redirect_to retailer_products_page, :notice => "#{product.name} has been removed from archive!"
    end

  end

  def new
    @product = Product.new

    respond_to do |format|
      format.html { render :action => :new, :layout => false }
    end
  end

  def finish
    product = Product.find_by_permalink(params[:id])

    # All product will be started from the index page!
    product.set_meta_data
    product.product_idle_reset
    product.save
    flash.notice = "Thank You - Your product is ready, click the start button when ready to start!"
    redirect_to retailer_products_page

  end

  def start

    product = Product.find_by_permalink(params[:id])

    respond_to do |format|
      if product.product_ready_reset
        format.html { redirect_to :back, :notice => flash.notice = "Thank You - Your product has been listed and will go live at #{product.available_on.to_formatted_s(:long)}!"}
      else
        format.html { redirect_to :back, :error => flash.notice = "There was an error processing your product, edit and please try again!"}
      end
    end

  end

  def toggle_relist

    product = Product.find(params[:id])
    product.toggle! :relist

    render :nothing => true

  end


  def create

    @product = Product.new(params[:product])

    @product.retailer_id = current_user.id

    @product.state = "idle"
    @product.overseas = false

    respond_to do |format|

      if @product.save
        format.html { redirect_to retailer_product_images_url(@product) }
      else
        logger.info("Theres a problem here saving available_on.... WHY; it saves when set in the model..")
        format.html { render :action => :new, :layout => !request.xhr?, :notice => "There was a problem creating the product" }
      end
    end

  end


  # override the destory method to set deleted_at value
  # instead of actually deleting the product.
  def destroy
    @product = Product.find_by_permalink(params[:id])
    @product.deleted_at = Time.now()

    @product.variants.each do |v|
      v.deleted_at = Time.now()
      v.save
    end

    if @product.save
      flash.notice = I18n.t("notice_messages.product_deleted")
    else
      flash.notice = I18n.t("notice_messages.product_not_deleted")
    end

    respond_to do |format|
      format.html { redirect_to collection_url }
      format.js { render_js_for_destroy }
    end
  end

  def clone
    load_object
    @new = @product.duplicate

    if @new.save
      flash.notice = I18n.t("notice_messages.product_cloned")
    else
      flash.notice = I18n.t("notice_messages.product_not_cloned")
    end

    redirect_to edit_retailer_product_url(@new)
  end

  # Allow different formats of json data to suit different ajax calls
  def json_data
    json_format = params[:json_format] or 'default'
    case json_format
      when 'basic'
        collection.map { |p| {'id' => p.id, 'name' => p.name} }.to_json
      else
        collection.to_json(:include => {:variants => {:include => {:option_values => {:include => :option_type}, :images => {}}}, :images => {}, :master => {}})
    end
  end


  def load_data
    @tax_categories = TaxCategory.order(:name)
    @shipping_categories = ShippingCategory.order(:name)
  end

  def collection
    return @collection if @collection.present?

    products = current_user.products.order("id ASC")

    unless request.xhr?
      params[:search] ||= {}
      # Note: the MetaSearch scopes are on/off switches, so we need to select "not_deleted" explicitly if the switch is off
      if params[:search][:deleted_at_is_null].nil?
        params[:search][:deleted_at_is_null] = "1"
      end

      params[:search][:meta_sort] ||= "name.asc"

      @search = products.metasearch(params[:search])

      pagination_options = {:include => {:variants => [:images, :option_values]},
                            :per_page => Spree::Config[:admin_products_per_page],
                            :page => params[:page]}

      @collection = @search.paginate(pagination_options)

    else
      includes = [{:variants => [:images, {:option_values => :option_type}]}, :master, :images]

      @collection = end_of_association_chain.where(["name LIKE ?", "%#{params[:q]}%"])
      @collection = @collection.includes(includes)


      tmp = end_of_association_chain.where(["variants.sku LIKE ?", "%#{params[:q]}%"])
      tmp = tmp.includes(:variants_including_master)
      @collection.concat(tmp)

      @collection.uniq
    end

  end


  def index_before
    session[:back_to_page] = params[:page].present? ? params[:page] : nil
  end


  def update_before
    # note: we only reset the product properties if we're receiving a post from the form on that tab
    return unless params[:clear_product_properties]
    params[:product] ||= {}
  end

  def next_url
    if params[:commit] == "Finish"
      retailer_products_page
    else
      retailer_product_images_path(@product)
    end
  end


end