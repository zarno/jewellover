class Retailer::RegistrationsController < UserRegistrationsController


  def new

    @retailer = Retailer.new
    @retailer.build_retailer_detail
    @retailer.stores.build

    @retailer.authentication_token = Retailer.generate_token(:authentication_token)

  end


  def create

    @retailer = build_resource(params[:retailer])


    if resource.save && params[:agreement]

      RetailerMailer.aweber_subscription(resource).deliver
      RetailerMailer.registration_confirmation(resource).deliver


      resource.create_watchlist
      resource.roles.clear # Get rid of "user" role and add "retailer" role
      resource.assign_role("retailer")

      after_sign_in_path_for(@retailer)

      set_flash_message(:notice, :signed_up)
      sign_in_and_redirect(:user, @retailer)

    else

      clean_up_passwords(resource)
      render_with_scope(:new)

    end

  end

  def test_email
    if user_signed_in?
      RetailerMailer.registration_confirmation(current_user).deliver
    end
    flash[:notice] = "Test rego email sent to #{current_user.email}"
    redirect_to retailer_path
  end


  private

  def after_sign_in_path_for(resource)
    retailer_path
  end

end