class Retailer::RetailerDetailsController < Retailer::BaseController



  def edit
    @retailer_detail = RetailerDetail.find_by_retailer_id(current_user.id)
  end



  def update
    @retailer_detail = RetailerDetail.find_by_retailer_id(current_user.id)

    respond_to do |format|

      if @retailer_detail.update_attributes(params[:retailer_detail])
        format.html {redirect_to edit_retailer_retailer_detail_path }
        flash.notice = "Your account details have been saved successfully"
      else
        format.html {render edit_retailer_retailer_detail_path }
        flash.alert = "There was a problem saving your details"
      end

    end

  end



end