class Retailer::SettingsController < Retailer::BaseController

  skip_authorize_resource

  def show
    authorize! :show, :settings
  end


end