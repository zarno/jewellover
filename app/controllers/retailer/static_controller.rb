class Retailer::StaticController < Retailer::BaseController

  skip_authorize_resource

  def support
    @faqs = Post.joins(:post_role).where("name = ?", "Retailer FAQS")
  end

end