class Retailer::StoresController < Retailer::BaseController

  def index
    @stores = Retailer.find(current_user).stores
  end

  def new
    @store = Store.new
  end

  def create
    @store = Store.new(params[:store])
    @store.retailer_id = current_user.id

    respond_to do |format|
      if @store.save
        format.html { redirect_to retailer_stores_path }
        flash.notice = "Your new store has been saved successfully"
      else
        format.html { render new_retailer_store_path }
        flash.alert = "There was a problem saving your store"
      end
    end
  end

  def edit
    @store = Store.find_by_permalink(params[:id])
  end

  def update
    @store = Store.find_by_permalink(params[:id])

    respond_to do |format|
      if @store.update_attributes(params[:store])
        format.html { redirect_to retailer_stores_path }
        flash.notice = "Your new store has been saved successfully"
      else
        format.html { render :edit }
        flash.alert = "There was a problem saving your store"
      end
    end
  end

  def destroy

    @store = Store.find_by_permalink(params[:id])

    # Cant delete a store when there is products associated with it,
    # Reset all the current retailer Products to default store
    default_store_id = Retailer.find(current_user).stores.first.id

    products = @store.products

    products.each do |p|
      p.update_attributes(:store_id => default_store_id)
    end unless products.empty?

    respond_to do |format|

      if @store.destroy
        format.html { render retailer_stores_path }
        flash.notice = "Store deleted successfully, all products associated with this store have now belong to your default original store "
      else
        format.html { render retailer_stores_path }
        flash.alert = "There was a problem destroying your store"
      end

    end


  end


end