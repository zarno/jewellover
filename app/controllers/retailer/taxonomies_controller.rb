class Retailer::TaxonomiesController < Retailer::BaseController
  resource_controller
  
  create.wants.html {redirect_to edit_retailer_taxonomy_url(@taxonomy)}
  update.wants.html {redirect_to collection_url}
  
  edit.response do |wants|
    wants.html
    wants.js do
      render :partial => 'edit.html.erb'
    end
  end

end
