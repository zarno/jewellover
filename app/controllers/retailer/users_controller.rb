class Retailer::UsersController < Spree::BaseController

  before_filter :authenticate_user!

  def show

    @retailer = Retailer.find(params[:id])

    respond_to do |format|
      format.html {redirect_to retailer_path}
      format.xml { render :xml => @retailer }
    end

  end

end