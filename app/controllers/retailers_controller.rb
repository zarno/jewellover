class RetailersController < Spree::BaseController

  def index

    @retailers = Retailer.all

  end

  def show

    @retailer = Retailer.find(1)

  end


end