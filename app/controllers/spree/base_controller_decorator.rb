Spree::BaseController.class_eval do

  def server_time
    server_time = {
        'time' => "#{Time.current.to_formatted_s(:rfc822)}"
    }

    respond_to do |format|
      format.json { render :json => server_time }
    end
  end

  def site_product_info

    site_product_info = {
        'total_product_value' => "#{Product.where_live.map { |p| p.price }.sum.round}"
    }

    respond_to do |format|
      format.json { render :json => site_product_info }
    end
  end

  def get_per_page
    cookies[:view_amount] ||= 30
  end

end