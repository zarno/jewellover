class StaticController < Spree::BaseController

  require 'active_support/secure_random'

  before_filter :authorize
  before_filter :setup_voucher, :only => :voucher

  def voucher

    cookies[:jl_slide_panel] = false

    respond_to do |format|
      format.html

      format.pdf do
        render :pdf => 'voucher', :template => 'static/voucher.erb'
      end
    end

  end


  private

  def authorize
    unless user_signed_in?
       redirect_to  login_path, :alert => "You must be logged in to redeem your voucher"
    end
  end

  def setup_voucher

    @expires = 1.month.from_now.strftime("%d/%m/%Y")
    @vid = ActiveSupport::SecureRandom.hex(10)


  end



end