class StoresController < Spree::BaseController

  def index
    @stores = Store.with_product_online
    @title = "Jewellery Stores"
    @meta_keywords = "discount, local, online jewellery, savings"
    @meta_description = "Find your favourite jewellery stores from around Australia on Jewellover."
    filter_stores(params[:sort_by], params[:state])
  end

  def show
    @store = Store.find_by_permalink(params[:id])
    @title = @store.name
    @meta_keywords = "jewellover, discount, online jewellery, rings, watches, necklace, gold, silver, jewellery,"
    @meta_description = @store.try(:description)
    @retailer = @store.retailer
    @products = @store.products
  end


  private


  def filter_stores(sort_by = nil, state = nil)

    if state && !state.empty?
      @stores = @stores.select { |s| s.state == state }
    end

    if sort_by == "products_lowest"
      @stores = @stores.sort { |x, y| y.products_count <=> x.products_count }
    else
      @stores = @stores.sort { |x, y| x.products_count <=> y.products_count }
    end

    @stores = @stores.paginate(:page => params[:page], :per_page => 5)

  end

end