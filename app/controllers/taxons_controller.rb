class TaxonsController < Spree::BaseController

  before_filter :load_data, :only => :show
  helper :products

  resource_controller

  show.before do
    @context = "in #{@taxon.name.capitalize}"
  end

  actions :show


  rescue_from ActiveRecord::RecordNotFound, :with => :render_404

  private

  def load_data
    @taxon ||= object
    render_404 and return if @taxon.nil?
    params[:taxon] = @taxon.id

    @search = @taxon.products.where_online.search(params[:search])

    if params[:sort_by]
      @products = @search.relation.instance_eval(params[:sort_by])
    else
      @products = @search.all
    end

    @products = @products.paginate(:page => params[:page], :per_page => get_per_page)

  end

  def object
    @object ||= end_of_association_chain.find_by_permalink("#{params[:id]}/")
  end

  def accurate_title
    @taxon ? @taxon.name : nil
  end

end