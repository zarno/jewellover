UserRegistrationsController.class_eval do

  after_filter :store_omniauth, :only => [:build_resource]

  def create

    @user = build_resource(params[:user])

    unless params[:redirect_to].blank?
      session[:user_return_to] = params[:redirect_to]
    end

    if resource.save

      UserMailer.delay(:queue => "Mailers").aweber_subscription(resource)

      after_sign_in_path_for(@user)

      set_flash_message(:notice, :signed_up)
      sign_in_and_redirect(:user, @user)

    else

      clean_up_passwords(resource)
      render_with_scope(:new)

    end

    session[:omniauth] = nil unless @user.new_record?

  end


  private


  def store_omniauth
    if session[:omniauth]
      @user.apply_omniauth(session[:omniauth])
      @user.valid?
    end
  end


end