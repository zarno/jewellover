class UserSessionsController < Devise::SessionsController

  include SpreeBase
  helper :users, 'spree/base'

  include Spree::CurrentOrder

  after_filter :associate_user, :only => :create

  ssl_required :new, :create, :destroy, :update
  ssl_allowed :login_bar


  def create
    authenticate_user!

    unless params[:redirect_to].blank?
      session[:user_return_to] = params[:redirect_to]
    end

    if user_signed_in?
      respond_to do |format|
        format.html {
          flash[:notice] = I18n.t("logged_in_succesfully")

          if current_user.has_role?("retailer")
            redirect_to retailer_path
          elsif current_user.has_role?("admin")
            redirect_to admin_path
          else
            redirect_back_or_default(my_dashboard_path)
          end
        }
        format.js {
          user = resource.record
          render :json => {:ship_address => user.ship_address, :bill_address => user.bill_address}.to_json
        }
      end
    else
      flash[:error] = I18n.t("devise.failure.invalid")
      render :new
    end

  end

  # GET /resource/sign_in
  def new
    super
  end

  def destroy
    session.clear
    super
  end

  def nav_bar
    render :partial => "shared/nav_bar"
  end

  private

  def associate_user
    return unless current_user and current_order
    current_order.associate_user!(current_user)
    session[:guest_token] = nil
  end

  def accurate_title
    I18n.t(:log_in)
  end


end