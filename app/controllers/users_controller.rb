class UsersController < Spree::BaseController

  before_filter :authenticate_user!

  def add_product_to_watchlist

    product = Product.find(params[:product_id])

    if current_user.watchlist.products.include? product
      flash.alert = "You are already watching #{product.name}"
      redirect_back_or_default(my_dashboard_path)
    else
      current_user.watchlist.products << product
      flash.notice = "You are now watching #{product.name}"
      redirect_back_or_default(my_dashboard_path)
    end

  end

  def remove_product_from_watchlist

    product = Product.find(params[:product_id])

    if current_user.watchlist.products.delete product
      flash.notice = "You have stopped watching #{product.name}"
      redirect_back_or_default(my_dashboard_path)
    else
      flash.notice = "There was a problem removing #{product.name} from your watchlist."
      redirect_back_or_default(my_dashboard_path)
    end

  end

  def edit
    @user = current_user
  end

  def update
    @user = User.find(params[:id])
    @user.user_preference.attributes = params[:user][:user_preference_attributes]

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to(my_account_path, :notice => 'Thank You - Your details have been saved successfully.') }
      else
        format.html { redirect_to(my_account_path, :alert => 'There was a problem saving your details, please try again.') }
      end
    end


  end

end