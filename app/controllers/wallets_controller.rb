class WalletsController < Spree::BaseController

  before_filter :authenticate_user!

  def show
    @wallet = Wallet.includes(:coupons).find_by_user_id(current_user)

    respond_to do |format|
      format.html # show.html.erb
    end
  end


end
