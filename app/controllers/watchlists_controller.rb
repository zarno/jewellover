class WatchlistsController < Spree::BaseController

  before_filter :authenticate_user!

  def index
    products = current_user.watchlist.products.select{|p| p.live? || p.upcoming?}
    @wproducts = products.paginate(:page => params[:page])
    @product_states = products.group_by { |p| p.retailer.stores.first.state }
  end

  def create

    @wproduct = Product.find(params[:id]) unless !user_signed_in?


    if current_user.watchlist.products << @wproduct

      redirect_to :back #watchlist_path
      flash.notice = "You are now watching #{@wpdroduct.name}"

    else

      redirect_to :back #watchlist_path
      flash.alert = "There was a problem adding this item to your watchlist, please try again."

    end

  end

  def destroy

    @wproduct = current_user.watchlist.products.find(params[:id])


    if current_user.watchlist.products.delete(@wproduct)

      flash.notice = "You have stopped watching #{@wpdroduct.name}"
      redirect_to watchlist_path

    else

      flash.alert = "There was a problem deleting this product from your watchlist, please try again."
      redirect_to watchlist_path


    end


  end

end
