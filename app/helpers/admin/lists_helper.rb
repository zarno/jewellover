module Admin::ListsHelper

  def list_actions(list)
    button_to "Destroy", admin_list_path(list), :method => :delete, :confirm => "Are you sure?"
  end

end
