module Admin::ProductsHelper

  def admin_actions(product)
    content_tag :ul do
      if product.expired? || product.idle?
        concat content_tag :li, (link_to_with_icon "relist", "Start", reset_admin_product_path(product), :class => "edit")
        concat content_tag :li, (link_to_edit product, :class => 'edit')
      elsif product.sold?
        concat content_tag :li, (link_to_with_icon "page", "View Order", edit_admin_order_path(product), :class => "edit")
      elsif product.cart?
        content_tag :li, (link_to_with_icon "page", "View Order", edit_admin_order_path(product), :class => "edit")
      elsif product.live? || product.upcoming?
        concat content_tag :li, (link_to_with_icon "email", "Email Campaign", "javascript:void(0);", :class => "panel_trigger")
      end
    end
  end

  def store_address(store)
    addy = []

    addy << store.address1
    addy << store.address2 unless store.address2.empty?
    addy << store.city
    addy << store.state
    addy << store.country

    addy * "<br/>"

  end


end