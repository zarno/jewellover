module ApplicationHelper

  def search_form(search, context=nil)
    render :partial => 'shared/forms/search/products', :locals => {:context => context, :search => search}
  end


  #If the user isnt signed in add the overlay signin class for overlay -- assigned to Login Button
  def auth_trigger
    unless user_signed_in?
      "overlay_trigger"
    end
  end

  def context_header
    if params[:search].present?
      h1 = "Search results for '#{params[:search][search_method]}'"
      if @taxon.present?
        h1 += " in #{@taxon.name}"
      elsif @store.present?
        h1 += " in #{@store.name}"
      elsif @current_action == "upcoming"
        h1 += " in Upcoming Deals"
      end
      h1
    elsif @taxon.present?
      "#{@taxon.name}"
    elsif @store.present?
      "#{@store.name}"
    elsif @current_action == "upcoming"
      "Upcoming Deals"
    end
  end

  def search_method
    if @store.present?
      :where_store_products_name_taxon_contains
    elsif @taxon.present?
      :where_taxon_products_name_store_contains
    else
      :where_name_store_taxon_contains
    end
  end


  def h3_with_icon(name, image = nil)
    if image
      content_tag :h3, :class => "icon", :style => "background-image:url('#{image}');" do
        name
      end
    else
      content_tag :h3 do
        name
      end
    end
  end

  def jl_get_taxonomies
    @taxonomies ||= Taxonomy.includes(:taxons).where('taxons.parent_id IS NOT NULL')
  end


  def watchlist_btn(item)

    params[:controller] == "dashboard" ? btn = "Remove" : btn = "Remove from watchlist"

    if user_signed_in? && !current_user.has_role?("admin")
      if current_user.watchlist.products.include? item
        button_to btn, {:controller => "users", :action => "remove_product_from_watchlist", :product_id => item.id}, :class => "watchlist add"
      else
        button_to "Add to watchlist", {:controller => "users", :action => "add_product_to_watchlist", :product_id => item.id}, :class => "watchlist add #{auth_trigger}", :rel => "#both"
      end unless current_user.has_role? "retailer"
    else
      button_to "Add to watchlist", {:controller => "users", :action => "add_product_to_watchlist", :product_id => item.id}, :class => "watchlist add #{auth_trigger}", :rel => "#both"
    end

  end

  def breadcrumber
    crumbs = {}

    request.fullpath.split("/").each do |path|
      crumbs[path.split("?")[0]] = request.fullpath unless path.empty?
    end

    content = content_tag :ul do
      concat content_tag(:li, "You are here:")
      concat content_tag(:li, link_to("Home", root_path) + raw("&nbsp;&raquo;"))
      crumbs.each_with_index do |(k, v), i|
        if i == crumbs.length - 1
          concat content_tag(:li, truncate(k.gsub(/[-_]/, " ").capitalize, :length => 65))
        else
          if k == "t"
            next # /t/ is custom routing for dynamic taxons ie. Categories, Brands
          elsif k == "categories"
            concat content_tag(:li, link_to("Categories", categories_path) + raw("&nbsp;&raquo;"))
          else
            concat content_tag(:li, link_to(k.gsub(/[-_]/, " ").capitalize, v.split("/")[0..(i+1)].join("/")) + raw("&nbsp;&raquo;"))
          end
        end
      end
    end

    concat content

  end


  def breadcrumbs_orig(taxon, separator="&nbsp;&raquo;&nbsp;")
    return "" if current_page?("/")
    separator = raw(separator)
    crumbs = [content_tag(:li, link_to(t(:home), root_path) + separator)]
    if taxon
      crumbs << content_tag(:li, link_to(t('products'), products_path) + separator)
      crumbs << taxon.ancestors.collect { |ancestor| content_tag(:li, link_to(ancestor.name, seo_url(ancestor)) + separator) } unless taxon.ancestors.empty?
      crumbs << content_tag(:li, content_tag(:span, taxon.name))
    else
      crumbs << content_tag(:li, content_tag(:span, t('products')))
    end
    crumb_list = content_tag(:ul, raw(crumbs.flatten.map { |li| li.mb_chars }.join))
    content_tag(:div, crumb_list + tag(:br, {:class => 'clear'}, false, true), :class => 'breadcrumbs')
  end


# Retrieves the collection of products to display when "previewing" a taxon.  This is abstracted into a helper so
# that we can use configurations as well as make it easier for end users to override this determination.  One idea is
# to show the most popular products for a particular taxon (that is an exercise left to the developer.)
  def taxon_preview(taxon, max=4)
    products = taxon.active_products.limit(max)
    if (products.size < max) && Spree::Config[:show_descendents]
      taxon.descendants.each do |taxon|
        to_get = max - products.length
        products += taxon.active_products.limit(to_get)
        break if products.size >= max
      end
    end
    products
  end

  def seo_link_to(object)
    # Dont forget controller must search by name!
    "/#{object.class.to_s.downcase}s/#{object.name}"
  end

  def highest_bidder(product)

    if product.bids.empty?
      "No offers yet!"
    else
      if product.highest_bidder == current_user
        "You"
      else
        product.highest_bidder.username
      end
    end

  end

  def clippy(text, bgcolor='#FFFFFF')
    html = <<-EOF
    <object style="margin-top:5px;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
            width="150"
            height="20"
            id="clippy" >
    <param name="movie" value="/images/common/clippy.swf"/>
    <param name="allowScriptAccess" value="always" />
    <param name="quality" value="high" />
    <param name="scale" value="noscale" />
    <param NAME="FlashVars" value="text=#{text}">
    <param name="bgcolor" value="#{bgcolor}">
    <embed src="/images/common/clippy.swf"
           width="150"
           height="20"
           name="clippy"
           quality="high"
           allowScriptAccess="always"
           type="application/x-shockwave-flash"
           pluginspage="http://www.macromedia.com/go/getflashplayer"
           FlashVars="text=#{text}"
           bgcolor="#{bgcolor}"
    />
    </object>
    EOF

    html.html_safe

  end


  def link_to_orders
    text = "Shopping Cart"
    orders = current_user.orders

    if !orders.empty?
      orders.select! { |o| !%w(complete canceled refunded).include? o.state }
      price = 0
      counter = 0
      orders.each_with_index do |o, i|
        price += o.total.to_f
        counter = i + 1
      end
      text += " (#{counter}) #{format_price(price)}"
    else
      text += " (None)"
    end

    link_to text, orders_path

  end

  def more_info(info, about=nil)
    c = "<span class=\"more_info\"></span>".html_safe
    c += "<small class=\"tooltip info\">#{info.html_safe}</small>".html_safe
    c += about
  end

  def store_address(store, style = 'tall')
    addy = []

    addy << store.address1
    addy << store.address2 unless store.address2.empty?
    addy << store.city
    addy << "#{store.state}, #{store.postcode}"
    addy << store.country

    if style == 'tall'
      addy * "<br/>"
    elsif style == 'short'
      addy * ", "
    end

  end

  def site_total_value
    Product.where_live.map { |p| p.price }.sum
  end

  def https_url(url)
    protocol = url.split(":")
    protocol.shift
    protocol.unshift("https")
    protocol.join(":")
  end


end
