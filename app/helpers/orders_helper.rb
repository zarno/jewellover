module OrdersHelper


  # NOT IN USE
  def state_explination(state)
    msg = nil
    case state
      when "complete"
        msg = "Complete orders are orders that you have successfully paid for.
              Once complete all you have to do is wait for the product to arrive."
      when "cart"
        msg = "These orders you have yet to pay for, view each order below and complete the purchase."
      when "delivery"
        msg = "We are still waiting for you to set the delivery method and complete the purchase of these products."
      when "payment"
        msg = "."
    end

    return msg
  end

  def checkout_button(order)

    if order.state == "cart"
      link = link_to "Checkout NOW!", checkout_path(:order_id => order.number), :class => "button primary checkout"
    else
      link = link_to "Checkout NOW!", edit_order_checkout_path(order, :order_id => order.number), :class => "button primary checkout"
    end

    return link
  end

end