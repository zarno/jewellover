ProductsHelper.class_eval do

  def reserve_met?(product)
    unless product.bids.empty?
      image_tag("common/flaggreen.png", :style => "border:none; height:16px; float:left;") + ("<span style=\"color:#0a0;\"> Reserve met.</span>").html_safe()
    end
  end

  def product_image_url(product)
    if product.images.first.nil?
      root_url + "images/noimage/small.jpg"
    else
      product.images.first.attachment.url(:small)
    end
  end

  def product_reserve(product)
    if product.bids.count == 0
      "Reserve: #{format_price(product.cost_price)}"
    else
      "Current Offer: #{format_price(product.highest_bid.amount)}"
    end
  end

  def render_cloud_zoom(product, width=nil)
    if width && product.images.first.attachment_width > width
      link_to "#{product.images.first.attachment.url(:original)}",
              :class => "cloud-zoom",
              :id => "zoom1",
              :rel => "position:'inside'" do
        large_image(product)
      end
    else
      large_image(product)
    end
  end

  def render_view_style(products)
    if cookies[:view_style].present? && cookies[:view_style] == "list_view"
      render '/products/view_list_items', :products => products
    else
      render '/products/view_grid_items', :products => products
    end
  end

  def search_url
    if @store.present?
      path = store_products_path(@store)
    elsif @taxon.present?
      path = nested_taxons_path(@taxon.permalink)
    elsif @current_action == "upcoming"
      path = products_path + "?upcoming"
    else
      path = products_path
    end
    path
  end

end