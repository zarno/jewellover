module RegistrationsHelper

  def token
    @retailer.authentication_token ||= params[:retailer][:authentication_token]
  end

end