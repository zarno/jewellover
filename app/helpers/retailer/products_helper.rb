module Retailer::ProductsHelper

  def product_actions(product)
    content_tag :ul do
      if product.deleted?
        concat content_tag :li, (button_to "Unarchive", archive_retailer_product_path(product))
      else
        if product.expired? || product.idle?
          if current_user.has_role? "admin"
            concat content_tag :li, (link_to_with_icon "relist", "Start", start_admin_product_path(product), :class => "edit start")
          else
            concat content_tag :li, (link_to_with_icon "relist", "Start", start_retailer_product_path(product), :class => "edit start")
          end
          concat content_tag :li, (link_to_edit product, :class => 'edit')
          concat content_tag :li, (button_to "Archive", archive_retailer_product_path(product), :confirm => "Are you sure you wish to archive this product?")
        elsif product.sold?
          concat content_tag :li, (button_to "Issue Refund", refund_retailer_product_path(product), :confirm => "Are you sure you wish to refund this item?")
          if product.order.present?
            concat content_tag :li, (link_to_with_icon "page", "View Order", edit_retailer_order_path(product.order), :class => "edit")
          end
        elsif product.cart?
          if product.order.present?
            content_tag :li, (link_to_with_icon "page", "View Order", edit_retailer_order_path(product.order), :class => "edit")
          end
        elsif product.live? || product.upcoming?
          content_tag :li, (content_tag :p, "Not editable now")
        end
      end
    end
  end

  def product_price(product_or_variant, options={})
    options.assert_valid_keys(:format_as_currency, :show_vat_text)
    options.reverse_merge! :format_as_currency => true, :show_vat_text => Spree::Config[:show_price_inc_vat]

    amount = product_or_variant.price
    amount += Calculator::Vat.calculate_tax_on(product_or_variant) if Spree::Config[:show_price_inc_vat]
    options.delete(:format_as_currency) ? format_price(amount, options) : amount
  end

  # Based on the product state, nothing but 'live' and 'upcoming' products should have dynamic counting
  def product_start_time(product)
    if product.live? || product.upcoming?
      concat distance_of_time_in_words_to_now(product.available_on)
      product.available_on > Time.now ? " from now" : " ago"
    else
      if product.available_on
        product.try(:available_on).to_formatted_s(:short)
      else
        "N/A"
      end
    end
  end

  def product_time_left(product)
    if product.live? || product.upcoming?
      distance_of_time_in_words_to_now(product.kill_time) + " to go"
    else
      "N/A"
    end
  end

  def product_current_price(product)
    if product.live? || product.upcoming?
      format_price(product.current_price)
    else
      "N/A"
    end
  end


end

