Spree::BaseHelper.module_eval do

  def resource_name
    :user
  end

  def resource
    @user ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end



  def meta_data_tags
    # Product Data Tags
    if self.respond_to?(:object) && object
      "".tap do |tags|
        if object.respond_to?(:meta_keywords) and object.meta_keywords.present?
          tags << tag('meta', :name => 'keywords', :content => object.meta_keywords) + "\n"
        end
        if object.respond_to?(:meta_description) and object.meta_description.present?
          tags << tag('meta', :name => 'description', :content => object.meta_description) + "\n"
        end
      end
    else
      keywords = title.split(" ")
      keywords << @meta_keywords.split(",") if @meta_keywords
      "".tap do |tags|
        tags << tag('meta', :name => "keywords", :content => keywords.join(",").downcase) + "\n"
        if @meta_description
          tags << tag('meta',
                      :name => "description",
                      :content => truncate(@meta_description,
                                           :length => 200) + "... Only at JewelLover")
        end
      end

    end
  end

  def available_countries
    return Country.order("id ASC") unless zone = Zone.find_by_name(Spree::Config[:checkout_zone])
    zone.country_list
  end


end