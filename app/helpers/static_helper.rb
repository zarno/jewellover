module StaticHelper
  def pdf_image_tag(image, options = {})
    options[:src] = File.expand_path(Rails.root) + '/public/images/' + image
    tag(:img, options)
  end
end