module WalletsHelper


  def coupon_status(coupon)
    if coupon.used_at.present?
      content_tag :span, :style => "color:red" do
        link_to "Redeemed", order_path(coupon.order)
      end
    elsif coupon.expires_at < Time.now
      content_tag :span, :style => "color:red" do
        "Expired"
      end
    elsif coupon.order.present?
      content_tag :span, :style => "color:blue" do
        link_to "In Use", order_path(coupon.order)
      end
    else
      content_tag :span, :style => "color:green" do
        "Valid"
      end
    end
  end

end
