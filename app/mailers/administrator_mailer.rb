class AdministratorMailer < ActionMailer::Base
  layout 'email/standard'
  helper 'spree/base'

  def registration_confirmation(retailer)
    @retailer = retailer
    mail(:to => retailer.email, :subject => "Registration Confirmation")
  end

  def aweber_subscription(retailer)
    @retailer = retailer
    mail(:to => "retaileregister@aweber.com", :subject => "Subscribe #{retailer.email} to Regdretailer")
  end

  def store_contact(retailer, user, content)
    @retailer = retailer
    @user = user
    mail(:to => retailer.email, :subject => "Contact from JewelLover", :from => user.email)
  end

  def product_purchased(retailer, product)
    @product = product
    @retailer = retailer
    mail(:to => retailer.email, :subject => "Contact from JewelLover", :from => user.email)
  end

  def product_campaign(user, product, message, subject)
    @product = product
    @user = user
    @message = message
    mail(:to => user, :subject => subject)
  end

end
