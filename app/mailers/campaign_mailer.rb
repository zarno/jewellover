class CampaignMailer < ActionMailer::Base

  layout 'email/standard'

  def coupon_advisor(email)
    mail(:to => email, :subject => "Your FREE JewelLover coupon is now ready to use!")
  end

  def coupon_reminder(email)
    mail(:to => email, :subject => "Time is running out to use your $10 coupon!")
  end

  def coupon_second_reminder(email)
    mail(:to => email, :subject => "Only days left to use your $10 coupon!")
  end

  def coupon_final_reminder(email)
    mail(:to => email, :subject => "Only Hours Left To Use Your Coupon")
  end



end