class OfferMailer < ActionMailer::Base
  layout 'email/standard'

  def offer_placed(user, product)
    @product = product
    @user = user
    mail(:to => user.email, :subject => "Thank-you - Your offer has been placed on JewelLover")
  end

  def offer_won(user, product)
    @product = product
    @user = user
    mail(:to => user.email, :subject => "Congratulations, Your offer on an item on JewelLover has been successful.")
  end

  def offer_beaten(user, product)
    @product = product
    @user = user
    mail(:to => user.email, :subject => "Uh Oh! Your offer on JewelLover has been beaten.")
  end
end
