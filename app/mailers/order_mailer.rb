class OrderMailer < ActionMailer::Base
  helper "spree/base"
  layout 'email/standard'

  def confirm_email(order, resend=false)
    @order = order
    subject = (resend ? "[RESEND] " : "")
    subject += "#{Spree::Config[:site_name]} #{t('subject', :scope =>'order_mailer.confirm_email')} ##{order.number}"
    mail(:to => order.email,
         :subject => subject)
  end

  def confirm_retailer_email(order, resend=false)
    @order = order
    subject = (resend ? "[RESEND] " : "")
    subject += "#{Spree::Config[:site_name]} #{t('subject', :scope =>'order_mailer.confirm_email')} ##{order.number}"
    mail(:to => [order.retailer.email, JEWELLOVER_CONFIG['admin_email']],
         :subject => subject)
  end


  def cancel_email(order, resend=false)
    @order = order
    subject = (resend ? "[RESEND] " : "")
    subject += "#{Spree::Config[:site_name]} #{t('subject', :scope => 'order_mailer.cancel_email')} ##{order.number}"
    mail(:to => order.email,
         :subject => subject)
  end

  def new_order(user, product)
    @product = product
    @user = user
    mail(:to => user.email, :subject => "Your have an order waiting for your purchase.")
  end

end
