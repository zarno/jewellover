class ResponseMailer < ActionMailer::Base

  layout 'email/standard'

  def recipient_response(email)
    mail(:to => email, :subject => "Your JewelLover deals are on their way!")
  end

end
