class RetailerMailer < ActionMailer::Base
  layout 'email/standard', :except => :aweber_subscription
  helper ApplicationHelper

  def registration_confirmation(retailer)
    @retailer = retailer
    mail(:to => [@retailer.email, JEWELLOVER_CONFIG['admin_email']],
         :subject => "Registration Confirmation")
  end

  def aweber_subscription(retailer)
    # Send to awber address for parsing! retailerregister list
    @retailer = retailer
    mail(:to => "retaileregister@aweber.com", :subject => "Subscribe #{@retailer.email} to retailerregister")
  end

  def product_purchased(retailer, product)
    @product = product
    @retailer = retailer
    mail(:to => retailer.email,
         :subject => "Congratulations, one of your product has been sold!")
  end

  def product_relisted(retailer, product)
    @product = product
    @retailer = retailer
    mail(:to => retailer.email,
         :subject => "One of your products has been relisted")
  end

  def order_refunded(order)
    @product = order.product
    @retailer = order.retailer
    @user = order.user
    @order = order
    mail(:to => [@retailer.email, JEWELLOVER_CONFIG['admin_email']],
         :subject => "Order #{@order.number} has been refunded to #{@user.username}")
  end

  def contact_message(message, store)

    debugger

    @message = message
    @store = store
    mail(:to => [@store.retailer.email, JEWELLOVER_CONFIG['admin_email']],
         :subject => "Store Inquiry from JewelLover!"
    )
  end

end
