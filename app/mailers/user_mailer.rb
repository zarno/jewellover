class UserMailer < ::Devise::Mailer

  layout 'email/standard'

  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Welcome to Jewellover")
  end

  def aweber_subscription(user)
    @user = user
    mail(:to => "jewellovers@aweber.com", :subject => "Subscribe #{user.email} to Jewellover")
  end

  def test_email(email)
    mail(:to => email, :subject => "Test Delayed Email Product from Jewellover")
  end

  def first_warning(user)
    @user = user
    mail(:to => user.email, :subject => "Please complete your purchase on JewelLover.")
  end

  def second_warning(user)
    @user = user
    mail(:to => user.email, :subject => "WARNING! You have outstanding orders on JewelLover")
  end

  def final_warning(user)
    @user = user
    mail(:to => user.email, :subject => "FINAL WARNING! You have outstanding orders on JewelLover")
  end

  def account_suspended(user)
    @user = user
    mail(:to => user.email, :subject => "Your Jewellover account has been suspended.")
  end

  def order_refunded(order)
    @user = order.user
    @retailer = order.retailer
    @product = order.product
    @order = order
    mail(:to => @user.email, :subject => "Order #{@order.number} has been refunded from JewelLover.")
  end

  def reset_password_instructions(user)
    default_url_options[:host] = Spree::Config[:site_url]

    @edit_password_reset_url = edit_user_password_url(:reset_password_token => user.reset_password_token)

    mail(:to => user.email,
         :subject => Spree::Config[:site_name] + ' ' + I18n.t("password_reset_instructions"))
  end
end

