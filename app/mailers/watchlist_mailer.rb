class WatchlistMailer < ActionMailer::Base
  layout 'email/standard'

  def daily_watchlist(user)
    @user = user
    mail(:to => user.email, :subject => "You have items on your watchlist!")
  end
end

