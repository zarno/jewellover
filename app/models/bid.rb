class Bid < ActiveRecord::Base

  belongs_to :bidbucket, :counter_cache => true
  belongs_to :user

  validates :amount, :numericality => true


  def self.highest
    order("amount DESC").first
  end

  def product
    self.bidbucket.product
  end

end
