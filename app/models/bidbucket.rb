class Bidbucket < ActiveRecord::Base
  belongs_to :product
  has_many :bids, :dependent => :destroy

  def empty
    bids.clear
  end


end
