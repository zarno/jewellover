class Calculator::FreeShipping < Calculator
  preference :minimum_purchase_amount, :decimal, :default => 0
  preference :discount_shipping_fee, :decimal, :default => 0
  preference :standard_shipping_fee, :decimal, :default => 0

  def self.description
    "Shipping Discount"
  end

  def self.register
    super
    ShippingMethod.register_calculator(self)
  end


  def compute(object=nil)
    base = object.respond_to?(:total) ? object.total : object.order.total

    if base >= self.preferred_minimum_purchase_amount
      self.preferred_discount_shipping_fee
    else
      self.preferred_standard_shipping_fee
    end
  end
end