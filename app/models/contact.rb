class Contact < ActiveRecord::Base
  validates_presence_of :name, :subject, :message
  validates :email, :format => {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
end