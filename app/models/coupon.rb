class Coupon < ActiveRecord::Base

  include ActiveSupport::SecureRandom

  before_create :setup_coupon

  belongs_to :order
  has_one :adjustment, :as => :source

  belongs_to :wallet

  def self.where_valid
    where('used_at IS NULL AND expires_at > ? AND order_id IS NULL', Time.now)
  end

  def self.where_invalid
    where('used_at IS NOT NULL OR order_id IS NOT NULL OR expires_at < ?', Time.now)
  end

  def is_valid?
    self.expires_at > Time.now && self.used_at == nil
  end

  def is_expired?
    self.expires_at < Time.now
  end

  ## Mailer Tasks
  def self.alert_users
    where_valid.each do |coupon|

      if coupon.expires_at < 3.days.from_now
        CampaignMailer.delay(:queue => "Mailers").coupon_final_reminder(coupon.wallet.user.email)
      elsif coupon.expires_at < 10.days.from_now
        CampaignMailer.delay(:queue => "Mailers").coupon_second_reminder(coupon.wallet.user.email)
      elsif coupon.expires_at < 20.days.from_now
        CampaignMailer.delay(:queue => "Mailers").coupon_reminder(coupon.wallet.user.email)
      end

    end
  end

  private

  def setup_coupon
    self.number = SecureRandom.hex(10)
    self.expires_at = 30.days.from_now
  end

end
