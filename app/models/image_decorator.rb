Image.class_eval do
  validates_attachment_content_type :attachment, :content_type => ['image/jpg', 'image/pjpeg', 'image/x-png', 'image/jpeg', 'image/png', 'image/gif'],
      :message => "must be a .jpg, .png or .gif file type."
end