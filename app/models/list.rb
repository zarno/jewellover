class List < ActiveRecord::Base

  has_many :list_recipients
  has_many :recipients, :through => :list_recipients
  has_many :users, :through => :list_recipients

  def emails
    emails = self.users.map(&:email)
    emails + self.recipients.map(&:email)
  end


end
