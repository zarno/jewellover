class ListRecipient < ActiveRecord::Base
  belongs_to :recipient
  belongs_to :list
  belongs_to :user

  # Make (user/recipient) entity relationship available, used for tracking when contact joined the list
  def contact
    self.user.present? ? self.user : self.recipient
  end



end
