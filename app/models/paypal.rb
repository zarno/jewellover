class Paypal < ActiveRecord::Base

  belongs_to :retailer

  attr_accessible :login, :password, :signature

  validates_presence_of :login, :password, :signature


end
