class Post < ActiveRecord::Base

  belongs_to :post_role

  validates_presence_of :title, :content, :post_role_id, :author

  scope :testimonials, includes(:post_role).where("post_roles.name = ?", "Testimonials").where(:published => true)
  scope :whats_new, includes(:post_role).where("post_roles.name = ?", "Whats New").where(:published => true)

end
