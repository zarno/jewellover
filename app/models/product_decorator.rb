Product.class_eval do


  after_create :setup_product_dependencies, :set_defaults
  after_save :update_defaults

  attr_accessor :stock_id, :categories, :reserve_price

  validates :cost_price, :presence => true, :numericality => {:greater_than => 0.99, :maximum => 999999.99}
  validates :price, :numericality => {:greater_than => 0.99, :maximum => 999999.99}
  validates :description, :presence => true
  validates :interval, :presence => true
  validates :store_id, :presence => true

  #custom
  validate :reserve_price_accuracy

  belongs_to :retailer
  belongs_to :store, :counter_cache => true

  has_one :bidbucket, :dependent => :destroy
  has_many :bids, :through => :bidbucket
  has_and_belongs_to_many :watchlists


  ### ========= Product scopes ========= ###


  scope :with_includes, includes(:images, :master, :store, :bidbucket)
  scope :with_show_includes, includes(:variants, {:bidbucket => :bids})

  scope :where_online, with_includes.where("products.state = ? OR products.state = ?", "live", "upcoming") # Products Page
  scope :where_upcoming, with_includes.where(:state => "upcoming")
  scope :where_live, with_includes.where(:state => "live")
  scope :where_sold, with_includes.where(:state => "sold").order('updated_at DESC')

  scope :where_cart, where(:state => "cart")
  scope :where_idle, where(:state => "idle")
  scope :where_expired, where(:state => "expired")

  scope :from_store, lambda { |store| where(:store_id => store.id) }
  scope :from_state, lambda { |state| joins(:store).where("stores.state = ?", state) }
  scope :from_category, lambda { |taxon| joins(:taxons).where("taxons.name = ?", taxon) }


  ### ========== Product Search ======== ###
  search_methods :where_name_store_taxon_contains,
                 :where_taxon_products_name_store_contains,
                 :where_store_products_name_taxon_contains


  scope :where_name_store_taxon_contains, lambda { |search|
    joins(:taxons).where('products.name ILIKE ? OR products.description ILIKE ? OR stores.name ILIKE ? OR taxons.name ILIKE ?',
          "%#{search}%",
          "%#{search}%",
          "%#{search}%",
          "%#{search}%")
  }

  scope :where_taxon_products_name_store_contains, lambda { |search|
    joins(:taxons).where('products.name ILIKE ? OR products.description ILIKE ? OR stores.name ILIKE ?',
          "%#{search}%",
          "%#{search}%",
          "%#{search}%")
  }

  scope :where_store_products_name_taxon_contains, lambda { |search|
    joins(:taxons).where('products.name ILIKE ? OR products.description ILIKE ? OR taxons.name ILIKE ?',
          "%#{search}%",
          "%#{search}%",
          "%#{search}%")
  }


  ### ====== Product Sort Filters ========= ###
  scope :oldest, order("created_at ASC")
  scope :latest, order("created_at DESC")
  scope :lowest_price, joins(:master).order("price ASC")
  scope :highest_price, joins(:master).order("price DESC")
  scope :lowest_reserve_price, joins(:master).order("cost_price ASC")
  scope :highest_reserve_price, joins(:master).order("cost_price DESC")

  def self.lowest_current_price
    where_online.sort { |x, y| x.current_price <=> y.current_price }
  end

  def self.highest_current_price
    where_online.sort { |x, y| y.current_price <=> x.current_price }
  end

  def self.lowest_discount
    where_online.sort { |x, y| x.drop_count <=> y.drop_count }
  end

  def self.highest_discount
    where_online.sort { |x, y| y.drop_count <=> x.drop_count }
  end


  ##### ======================= Custom Validations -=====================================#####

  def reserve_price_accuracy
    if cost_price && (cost_price >= price)
      errors.add(:cost_price, "must be less than the Start price.")
    end
  end


  #  ### ================================  Calculators  ===================================== ###

  #---> Time since product went live in seconds
  def time_spent
    if available_on
      st = available_on.to_time # when started
      ct = Time.now.to_time # current time

      (ct - st).to_i # seconds since
    else
      Time.now.to_i
    end
  end

  #---> Amount of times the price has dropped ------ add % to emulate current discount percentage
  def drop_count
    if upcoming? || live?
      ts = time_spent
      it = interval.to_i * 60

      # Unless product has started, discount is 0
      (ts / it).to_i <= 0 ? 0 : (ts / it).to_i
    elsif cart? || sold?
      sp = price
      rp = sold_for
      da = drop_amount

      ((sp - rp) / da).ceil
    else
      0 # for the idle products
    end
  end

  def drop_amount
    c = price * 0.01
    c.round(2)
  end


  def max_drop_count

    rp = cost_price
    #cp = current_price
    p = price
    da = drop_amount

    ((p - rp) / da).to_i

  end

  def max_saving
    (max_drop_count * drop_amount).round(1)
  end

  #---> Dollar difference from start price
  def current_saving
    a = drop_count * drop_amount
    a.round(2)
  end

  #---> Actual current dollar value of price after discount
  def current_price
    a = price - current_saving
    if a > cost_price && available_on
      available_on > Time.now ? price.round(2) : a.round(2)
    else
      cost_price
    end
  end


  #---> Time until next price drop
  def time_until
    # Measure in seconds!!
    if available_on
      i = interval.to_i * 60 # - Interval
      dc = drop_count + 1 # - Next Drop Count
      st = available_on.to_time # - Start Time

      st + (dc * i) # The time the next drop should occur
    else
      Time.now
    end

  end

  # ===== >>> This when the job should run.. in sync with front end timer at final drop!
  def kill_time

    i = interval * 60 # Interval in seconds
    st = !self.available_on.nil? ? available_on : Time.now
    sp = price # Starting Price
    da = drop_amount # Value of each drop
    rp = self.bidbucket.bids_count > 0 ? self.bids.last.amount : self.cost_price # Reserve price = cost price or bid price

    pr = sp - rp # Price range = start_price - reserve_price
    dc = pr / da # drop_count = price_range / drop_amount
    tl = dc * interval # time_left = drop_count * interval(minutes)

    kt = st + tl.minutes

    #fdc = ((kt - available_on) / i).ceil + 1 # Shorthand
    #
    #i = interval * 60 # We are adding seconds not minutes
    tt = kt - st # Total time product alive for...
    dc = tt / i # Total drop count the product will endure for its calculated life...
    fdc = dc.ceil # The final drop count
    #
    at = (fdc * i).seconds
    #
    (st + at) - 1.second # - The product should die at this point in time, not the calculated kill_time!

  end

  #---> Time to Kill the product! Should be absolute
  def kill_time_orig

    i = interval # Interval in minutes
    st = !self.available_on.nil? ? available_on : Time.now
    sp = price # Starting Price
    da = drop_amount # Value of each drop
    rp = !self.bids.empty? ? self.bids.last.amount : self.cost_price # Reserve price = cost price or bid price

    pr = sp - rp # Price range = start_price - reserve_price
    dc = pr / da # drop_count = price_range / drop_amount
    tl = dc * i # time_left = drop_count * interval(seconds)


    #========= Actual Kill Time -=====#
    kt = st + tl.minutes # Kill time = Start time plus Time left

  end

  # ==== Product States ======= #

  def live
    self.state = "live"
    self.save
  end

  def live?
    self.state == "live"
  end

  def cart

    # TODO remove product from all watchlists

    self.update_attributes(:relist => false)
    self.state = "cart"
    self.save
  end

  def cart?
    self.state == "cart"
  end

  def sold
    self.update_attributes(:relist => false)
    self.state = "sold"
    self.save
  end

  def sold?
    self.state == "sold"
  end

  def expired
    self.state = "expired"
    self.save
  end

  def expired?
    self.state == "expired"
  end

  def upcoming
    self.state = "upcoming"
    self.save
  end

  def upcoming?
    self.state == "upcoming"
  end

  def idle
    self.state = "idle"
    self.save
  end

  def idle?
    self.state == "idle"
  end

  # ======= SCOPE METHODS ======= #

  # API Product Search ability
  def self.search_by_name_or_sku(search, retailer_id = nil)

    lsearch = "%#{search}%".downcase

    if retailer_id.nil?
      if !Product.where("LOWER(name) LIKE ?", lsearch).empty?
        products = Product.where("LOWER(name) LIKE ?", lsearch)
      else
        products = Variant.joins(:product).where("sku = ?", search).map { |v| v.product }
      end
    else
      if !Product.joins(:retailer).where("LOWER(name) LIKE ? and retailer_id = ?", lsearch, retailer_id).empty?
        products = Product.where("LOWER(name) LIKE ? and retailer_id = ?", lsearch, retailer_id)
      else
        products = Variant.joins(:product).where("sku = ? and retailer_id = ?", search, retailer_id).map { |v| v.product }
      end
    end

    products

  end


  # ----- Standard Class Methods =------#

  def highest_bid
    bidbucket.bids.order("created_at DESC").first unless bidbucket.bids_count == 0
  end

  def highest_bidder
    self.highest_bid.try(:user)
  end

  def offer_price
    self.bidbucket.bids_count.to_i > 0 ? self.bids.last.amount : self.cost_price.ceil
  end

  def offer_saving
    sp = self.price
    a = self.bidbucket.bids_count.to_i > 0 ? self.bids.last.amount : self.cost_price

    (((sp - a) / sp) * 100).round(0).to_i
  end


  def set_meta_data
    keywords = self.name.split(" ")
    # Keywords should include categorys, and word jewellery and product name
    taxons.each { |t| keywords.insert(0, t.name) }
    keywords.insert(0, "jewellery")

    self.meta_description = "See this #{self.name} on Jewellover drop in price every #{self.interval} minutes."
    self.meta_keywords = keywords.join(", ").to_s
    self.save

  end

  def archive
    if self.deleted?
      self.deleted_at = nil
    else
      self.deleted_at = Time.now
    end
    self.save
  end

  def update_kill_time
    self.kill_at = kill_time
  end

  def is_ready?
    !self.images.empty? && !self.taxons.empty? && !self.deleted?
  end

  def sold_for
    master.line_items.last.order.total
  end

  # Get latest order this product belongs to through line_items and such
  # remember no line_items = no order!
  def order
    self.master.line_items.last.order unless self.master.line_items.empty?
  end


  def product_idle_reset
    self.available_on ||= Time.now
    self.on_hand = 1

    job.delete unless job.nil?

    self.idle
  end

  # call reset function now
  def product_ready_reset
    # delete any jobs associated with product and create new
    # job.destroy unless job.nil?
    if self.is_ready?

      # Time has passed since they created the product but do not wish to set a future date, Just go with 1 minute from now
      if available_on.nil? || available_on < Time.current
        if relist_count > 0 && self.live? # if product is set to auto-relist upcoming lasts 2 hour
          self.available_on = 1.hour.from_now
        else
          self.available_on = 1.minute.from_now
        end
      end

      self.on_hand = 1
      self.upcoming

      # Set job to set live state to run at changeover point
      self.product_golive
    else

      product_idle_reset

    end
  end

  def product_hard_reset
    if self.is_ready?
      # Only set the time if they havent chosen a time in the future.
      if available_on.nil? || available_on < Time.current
        self.available_on = 1.minute.from_now
      end
      self.upcoming

      # Set job to set live state to run at changeover point
      self.on_hand = 1

      self.product_golive
    else
      product_idle_reset
    end
  end


  #========= ASYNCORNOUS JOBS =========#


  def job
    Delayed::Job.find_by_id(self.job_id)
  end

  # Set the job id that will be updated with each bid, and deleted if Buy Now
  def set_product_kill

    set_job = self.delay(:run_at => self.kill_at, :queue => 'products', :priority => 1).product_kill

    #job = product_kill # Product kill is a delayed job
    logger.info("Delayed Job from set_product_kill => #{job.inspect}")
    self.update_attributes(:job_id => set_job.id)
  end

  # Update current kill job to an ealier time relative to bid product
  def update_kill_job
    if job.update_attributes(:run_at => self.kill_at, :queue => 'products', :priority => 1)
      logger.info("Product Kill Job updated ============== Now runing in #{job.run_at.to_s}")
    end unless job.nil?
  end

  def delete_kill_job
    if job.destroy
      logger.info("Product Kill Job DESTROYED ==============>>> #{job.inspect}")
    end
  end

  #=========== ASYNC ACTIONS =========#

  # Kill the product based on its current state, the Delayed job must be kept in
  # mind throughout product manipulation.
  def product_kill

    self.relist_count = self.relist_count.succ
    self.save

    if self.bids.empty?
      #if auto relist selected for this product reset at time expiration if on stage
      if self.relist?
        #reset product and start again
        self.product_ready_reset
      else
        expired
      end
    else
      product_win
    end

  end

  # Set the product to live state
  def product_golive
    live

    # set job to kill at deadline - update and apply
    unless self.job
      set_product_kill
    end

    unless self.bids.empty?
      update_kill_job
      update_kill_time
    end

  end

  handle_asynchronously :product_golive,
                        :run_at => Proc.new { |p| p.available_on },
                        :queue => 'products',
                        :priority => 1

  # Win the product, associate bidders, notify everyone involved!
  def product_win
    # create the order with winners creds and product details
    user = self.highest_bidder
    variant = self.master
    retailer = self.retailer

    order = user.orders.create!(:retailer_id => retailer.id)
    order.add_variant(Variant.find(variant.id), 1, true)

    cart

    OfferMailer.delay(:queue => 'mailers').offer_won(user, self)

  end

  handle_asynchronously :product_win,
                        :queue => 'products',
                        :priority => 1


  private # =========================================================================== #


  # Setup  all the product dependencies ie Bidbucket
  def setup_product_dependencies
    self.create_bidbucket
  end


  # Basic default setup from retailer/products/new
  # convert description to be metadata
  def set_defaults
    self.on_hand = 1
    self.overseas = false
  end

  # When product updated configure kill_times/times over again
  def update_defaults
    update_kill_time
  end

end