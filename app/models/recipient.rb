class Recipient < ActiveRecord::Base
  has_many :list_recipients
  has_many :lists, :through => :list_recipients

  validates_presence_of :email

  def assign_to_list(list)
    self.lists << List.find_or_create_by_name(list)
  end



end
