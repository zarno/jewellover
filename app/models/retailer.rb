class Retailer < User


  has_one :retailer_detail, :dependent => :destroy

  has_many :products, :dependent => :destroy
  has_many :orders

  has_many :stores, :dependent => :destroy

  attr_accessible :stores_attributes, :retailer_detail_attributes, :username, :authentication_token

  accepts_nested_attributes_for :stores, :retailer_detail, :products


  # -------------------- Methods ----------------------- #

  def self.all
    joins(:roles).where(:roles => {:name => "retailer"})
  end


end