class RetailerDetail < ActiveRecord::Base

  belongs_to :retailer

  attr_accessible :pp_email, :trading_name, :abn, :facebook

  validates_presence_of :pp_email, :trading_name

  validates :abn,
            :presence => true,
            :length => {:in => 11..16},
            :numericality => true

end
