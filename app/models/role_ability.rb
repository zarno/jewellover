# Implementation class for Cancan gem.  Instead of overriding this class, consider adding new permissions
# using the special +register_ability+ method which allows extensions to add their own abilities.
#
# See http://github.com/ryanb/cancan for more details on cancan.
class RoleAbility

  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    # ==== OLD ===== #
    if user.has_role? "retailer"

      # --- Manage --- #
      can :manage, Product, :retailer_id => user.id
      can :manage, Order, :retailer_id => user.id
      can :show, :settings
      can :manage, Store, :retailer_id => user.id
      can :manage, Image
      can :update, RetailerDetail, :retailer_id => user.id
      can :manage, Taxon
      can :manage, Shipment

    end




  end

end