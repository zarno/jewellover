Shipment.class_eval do
  def after_ship
    inventory_units.each &:ship!
    ShipmentMailer.delay(:queue => 'mailers').shipped_email(self)
  end
end