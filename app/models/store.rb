class Store < ActiveRecord::Base

  belongs_to :retailer

  has_many :products

  attr_accessible :name, :description, :license, :address1, :address2, :suburb, :state, :city, :postcode, :country, :permalink,
                  :fname, :lname, :phone, :fax, :website, :logo, :logo_file_name, :logo_content_type, :logo_file_size, :logo_updated_at, :products_count

  validates_presence_of :name, :address1, :city, :postcode, :state, :country, :fname, :lname, :phone, :description

  validates :permalink, :presence => true, :uniqueness => true, :on => :create

  validates :logo_file_name, :presence => true, :on => :create

  validates :postcode,
            :length => {:is => 4}

  validates :name,
            :length => {:maximum => 50},
            :uniqueness => true

  validates :phone,
            :numericality => true

  validate :permalink_validity


  has_attached_file :logo,
                    :styles => {
                        :tiny => "50x50>",
                        :small => "150x150>"},
                    :default_style => :small,
                    :storage => :s3,
                    :s3_credentials => "#{::Rails.root.to_s}/config/s3.yml",
                    :path => "/assets/logos/:id/:style/:basename.:extension",
                    :default_url => "/images/noimage/no_logo.jpg"


  validates_attachment_presence :logo, :if => Proc.new { |store| !store.logo_file_name.blank? }
  validates_attachment_size :logo, :less_than => 2.megabytes
  validates_attachment_content_type :logo, :content_type => ['image/jpg', 'image/pjpeg', 'images/x-png', 'image/jpeg', 'image/png', 'image/gif'],
                                    :message => "must be a .jpg, .png or .gif file type."

  scope :with_product_online, joins(:products).
      where("products.state = ? OR products.state = ?", "live", "upcoming").uniq

  def to_param
    permalink
  end


  def address
    {
        :address1 => self.address1,
        :address2 => self.address2,
        :city => self.city,
        :postcode => self.postcode,
        :state => self.state,
        :country => self.country
    }
  end



  def url
    "#{Spree::Config[:site_url]}/stores/#{self.permalink}"
  end

  def contact
    {
        :first_name => self.store.fname,
        :last_name => self.store.lname,
        :phone => self.store.phone
    }
  end

  def store_location
    "#{self.name}, #{self.suburb}"
  end

  private

  def permalink_validity
    reg = /^[a-z]([a-z0-9_])*$/
    unless reg.match(self.permalink)
      errors.add(:permalink, "invalid Jewellover URL")
    end
  end



end
