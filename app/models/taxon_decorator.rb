Taxon.class_eval do

  has_attached_file :icon,
                    :styles => {:mini => '32x32>', :normal => '200x150>'},
                    :default_style => :normal,
                    :path => "/assets/taxons/:id/:style/:basename.:extension",
                    :default_url => "/images/noimage/product.jpg",
                    :storage => :s3,
                    :s3_credentials => "#{::Rails.root.to_s}/config/s3.yml"
end
