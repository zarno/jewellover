User.class_eval do

  has_many :authentications, :dependent => :delete_all

  after_create :setup_user
  search_methods :with_role_by_name

  has_one :watchlist, :dependent => :destroy
  has_one :user_preference, :dependent => :destroy

  has_one :wallet, :dependent => :destroy
  has_many :coupons, :through => :wallet

  has_many :list_recipients
  has_many :lists, :through => :list_recipients


  attr_accessor :first_name, :last_name, :postcode, :dob

  attr_accessible :username, :locked_at, :first_name, :last_name, :postcode, :dob
  accepts_nested_attributes_for :user_preference, :update_only => true

  has_many :bids, :dependent => :destroy

  validates_numericality_of :postcode, :allow_blank => true
  validates :username, :presence => true, :uniqueness => true

  default_scope :include => [:orders, :roles]

  scope :retailers, lambda { includes(:roles).where("roles.name" => "retailer") }
  scope :customers, lambda { includes(:roles).where("roles.name" => "user") }
  scope :admins, lambda { includes(:roles).where("roles.name" => "admin") }

  # Metasearch custom scope
  scope :with_role_by_name, lambda { |role| joins(:roles).where("roles.name" => role) }


  # Meta_sort custom sort scopes does like so <%= link_to @search, :role, "Role" %>
  scope :sort_by_role_asc, includes(:roles).order("roles.name ASC")
  scope :sort_by_role_desc, includes(:roles).order("roles.name DESC")


  def apply_omniauth(omniauth)

    if omniauth['provider'] == 'facebook'
      logger.info("Processing omniauth info into database...")
      self.email = omniauth['info']['email'] if email.blank?
      self.first_name = omniauth['info']['first_name'] if first_name.blank?
      self.last_name = omniauth['info']['last_name'] if last_name.blank?
      self.username = omniauth['info']['nickname'] if username.blank?
      self.dob = omniauth['extra']['raw_info']['birthday'] if dob.blank?

      self.password = Devise.friendly_token[0, 10]
      # ASsociated Model Data
      #self.build_user_preference(:dob => omniauth['raw_info']['birthday'])
    end

    authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
  end


  def toggle_ban
    if self.locked_at.nil?
      self.update_attributes(:locked_at => Time.now)
    else
      self.update_attributes(:locked_at => nil)
    end
  end


  def products
    Product.where(:retailer_id => self.id)
  end

  def assign_role(role)
    self.roles << Role.find_or_create_by_name(role)
  end

  def list_roles
    self.roles.map(&:name).join(", ")
  end

  def assign_to_list(list)
    self.lists << List.find_or_create_by_name(list)
  end

# user bid selections

  def current_bids
    self.bids.order("created_at DESC").uniq_by(&:product).select { |b| b.product.live? }
  end

  def lost_bids
    self.bids.order("created_at DESC").uniq_by(&:product).select { |b| (b.product.cart? || b.product.sold?) &&
        b.product.highest_bidder != self }
  end

  def won_bids
    self.bids.order("created_at DESC").uniq_by(&:product).select { |b| (b.product.cart? || b.product.sold?) &&
        b.product.highest_bidder == self }
  end


# ==== MAILER METHODS ===== #

# Check the user has paid for all her orders, if she hasnt, remind them day by day until her is account suspended.

  def one_day_late?
    orders.map do |o|
      o.created_at < 1.day.ago && o.created_at > 2.days.ago && %w(cart payment delivery address).include?(o.state)
    end.include? true
  end


  def two_days_late?
    orders.map do |o|
      o.created_at < 2.days.ago && o.created_at > 3.days.ago && %w(cart payment delivery address).include?(o.state)
    end.include? true
  end

  def three_days_late?
    orders.map do |o|
      o.created_at < 3.day.ago && o.created_at > 4.days.ago && %w(cart payment delivery address).include?(o.state)
    end.include? true
  end

  def four_days_late?
    orders.map do |o|
      o.created_at < 4.day.ago && %w(cart payment delivery address).include?(o.state)
    end.include? true
  end

  def has_valid_coupon?
    coupons.map do |c|
      c.expires_at > 1.month.ago
    end.include? true
  end

  def self.payment_warning
    customers.each do |u|
      if u.one_day_late?
        # User has orders that are 1 day late
        UserMailer.delay(:queue => 'mailers').first_warning(u)
      end

      if u.two_days_late?
        # User has orders that are 2 days late
        UserMailer.delay(:queue => 'mailers').second_warning(u)
      end

      if u.three_days_late?
       # User has orders that are 3 days late
        UserMailer.delay(:queue => 'mailers').final_warning(u)
      end

      if u.four_days_late?
        # User has orders more than 3 days late... Order cancelled
        u.orders.to_cancel.each(&:cancel)
      end

    end
  end

  def self.watchlist_alert
    customers.each do |u|
      unless u.watchlist.products.empty?
        WatchlistMailer.delay(:queue => 'mailers').daily_watchlist(u)
      end
    end
  end

  private

  def setup_user

    self.create_user_preference
    self.create_watchlist
    self.create_wallet
    self.wallet.coupons.create(:value => 10, :expires_at => 30.days.from_now)

    assign_role("user")
    assign_to_list("Daily Deals")
    assign_to_list("Customers")

    UserMailer.delay(:queue => 'mailers').registration_confirmation(self)

  end

end