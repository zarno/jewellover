class UserPreference < ActiveRecord::Base

  belongs_to :user



  def full_name
    "#{self.try(:fname)} #{self.try(:lname)}"
  end

end
