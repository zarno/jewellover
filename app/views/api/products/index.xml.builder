xml.instruct!
xml.products do
  @products.each do |product|

    xml.product :id => product.id do

      xml.stock_id product.sku
      xml.available_on product.available_on
      xml.certified product.certified
      xml.resizable product.resizable
      xml.second_hand product.second_hand
      xml.overseas product.overseas
      xml.relist product.relist
      xml.state product.state
      xml.size product.size
      xml.description product.description
      xml.name product.name
      xml.categories product.taxons.map { |t| t.name }.join ", "
      xml.price product.price
      xml.reserve_price product.cost_price
      xml.current_price product.current_price
      xml.drop_amount product.drop_amount, :type => "dollars"
      xml.drop_count_since_live product.drop_count

      xml.interval product.interval, :type => "minutes"
      xml.retailer product.retailer.stores.first.name, :id => product.retailer.id
      xml.store_id product.store.id

      xml.images do
        product.images.each do |image|
          xml.image :id => image.id do

            xml.name image.attachment_file_name
            xml.orig image.attachment.url(:product)
            xml.thumb image.attachment.url(:thumb)
            xml.size image.attachment.size
            xml.alt image.alt

          end
        end
      end


    end
  end
end


# ==== DEFAULT PRODUCT  STRUCTURE ======= #

# <product>
# <available-on type="datetime">2011-06-17T07:35:44+10:00</available-on>
# <certified type="integer">1</certified>
# <closetime type="datetime" nil="true"/>
# <count-on-hand type="integer">1</count-on-hand>
# <created-at type="datetime">2011-06-03T13:56:31+10:00</created-at>
# <deleted-at type="datetime" nil="true"/>
# <departments nil="true"/>
# <description>
# Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede. Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
# </description>
# <id type="integer">1</id>
# <interval type="integer">60</interval>
# <meta-description>Rolex Mens watchs</meta-description>
# <meta-keywords>Watch</meta-keywords>
# <name>Watch</name>
# <permalink>watch</permalink>
# <resizable type="integer">1</resizable>
# <retailer-id type="integer">1</retailer-id>
# <shipping-category-id type="integer" nil="true"/>
# <size>G-5</size>
# <state nil="true"/>
# <tax-category-id type="integer" nil="true"/>
# <updated-at type="datetime">2011-06-17T09:35:46+10:00</updated-at>
# </product>