xml.instruct!
xml.taxonomies do
  @taxonomies.each do |taxonomy|
    xml.tag! taxonomy.name.downcase do
      taxonomy.taxons.order("id ASC").each_with_index do |taxon, i|
        xml.category taxon.name, :id => taxon.id unless i == 0
      end
    end
  end
end
