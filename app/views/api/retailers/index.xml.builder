xml.instruct!
xml.retailers do
  @retailers.each do |retailer|
    xml.retailer(:id => retailer.id) do

      xml.username retailer.username
      xml.contact retailer.stores.first.fname+" "+retailer.stores.first.lname
      xml.trading_name retailer.retailer_detail.trading_name unless retailer.retailer_detail.nil?
      xml.stores do
        retailer.stores.each do |store|
          xml.store :id => store.id do

            xml.contact store.fname + "  " + store.lname
            xml.store_name store.name
            xml.address1 store.address1
            xml.address2 store.address2
            xml.suburb store.suburb
            xml.city store.city
            xml.postcode store.postcode
            xml.state store.state

            xml.store_product_count store.products.count
          end
        end
      end
      xml.retailer_product_count retailer.products.count


    end
  end
end
