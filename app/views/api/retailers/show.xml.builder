xml.instruct!
xml.retailer(:id => @retailer.id) do

  xml.username @retailer.username
  xml.contact @retailer.stores.first.fname+" "+@retailer.stores.first.lname
  xml.trading_name @retailer.retailer_detail.trading_name

  xml.stores do
    @retailer.stores.each do |store|
      xml.store :id => store.id do
        xml.shop_name store.name

        xml.address1 store.address1
        xml.address2 store.address2
        xml.suburb store.suburb
        xml.city store.city
        xml.postcode store.postcode
        xml.state store.state
        xml.country store.country

        xml.store_product_count store.products.count
      end
    end
  end

  xml.retailer_product_count @retailer.products.count

end


# ======= Default Retailer Structure ==== #


# <retailer>
# <authentication-token nil="true"/>
# <bill-address-id type="integer" nil="true"/>
# <created-at type="datetime">2011-06-03T12:48:09+10:00</created-at>
# <current-sign-in-at type="datetime">2011-06-20T16:09:00+10:00</current-sign-in-at>
# <current-sign-in-ip>127.0.0.1</current-sign-in-ip>
# <email>store@gcds.com.au</email>
# <encrypted-password>
# 95dca9abe0ce5e7269b97120cde5474449557f0502294d0caf212881863523db347178c0cd48ef173e061d66c1a94b52c0a37054fd204192e3546703846b449e
# </encrypted-password>
# <failed-attempts type="integer">0</failed-attempts>
# <id type="integer">1</id>
# <last-request-at type="datetime" nil="true"/>
# <last-sign-in-at type="datetime">2011-06-20T09:18:43+10:00</last-sign-in-at>
# <last-sign-in-ip>127.0.0.1</last-sign-in-ip>
# <leveredge-id nil="true"/>
# <leveredge-token nil="true"/>
# <locked-at type="datetime" nil="true"/>
# <login>store@gmail.com</login>
# <password-salt>aPWUC8hJcBi0TXjsZyfR</password-salt>
# <perishable-token nil="true"/>
# <persistence-token nil="true"/>
# <remember-created-at type="datetime" nil="true"/>
# <remember-token nil="true"/>
# <reset-password-token nil="true"/>
# <ship-address-id type="integer" nil="true"/>
# <sign-in-count type="integer">3</sign-in-count>
# <unlock-token nil="true"/>
# <updated-at type="datetime">2011-06-20T16:09:00+10:00</updated-at>
# <username>Store Retailer</username>
# </retailer>