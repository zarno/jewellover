# config/initializers/delayed_job_config.rb
Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay = 0.1
Delayed::Worker.max_attempts = 5
Delayed::Worker.max_run_time = 30.minutes
#Delayed::Worker.backend = :active_record