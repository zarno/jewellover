if defined?(Footnotes) && Rails.env.staging? || Rails.env.development?
  Footnotes.run!
end
