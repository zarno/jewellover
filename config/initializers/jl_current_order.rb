Spree::CurrentOrder.module_eval do


  # This should be overridden by an auth-related extension which would then have the
  # opportunity to associate the new order with the # current user before saving.
  def before_save_new_order
  end

  # This should be overridden by an auth-related extension which would then have the
  # opporutnity to store tokens, etc. in the session # after saving.
  def after_save_new_order
  end

  # The current incomplete order from the session for use in cart and during checkout
  def current_order(create_order_if_necessary = false)


    if params[:order_id]
      @current_order = Order.find_by_number(params[:order_id], :include => :adjustments)
      session[:order_id] = nil
      logger.info("=================>>>>>> CURRENT ORDER -- Params MODULE HIT order -> #{@current_order.inspect}")
    end


    if session[:order_id]
      @current_order = Order.find_by_id(session[:order_id], :include => :adjustments)
      logger.info("=================>>>>>> CURRENT ORDER -- Session MODULE HIT order -> #{@current_order.inspect}")
    end

    #if buy now was selected
    if create_order_if_necessary and (@current_order.nil? or @current_order.completed?)
      session[:order_id] = nil
      @current_order = Order.new
      before_save_new_order
      @current_order.save!
      after_save_new_order
      logger.info("=================>>>>>> CURRENT ORDER -- Create New MODULE HIT order -> #{@current_order.inspect}")
    end


    ##if product has been won via price offer!!
    #if create_order_if_necessary and (@current_order.nil? or @current_order.completed?) && !user_id.nil?
    #  @current_order = Order.new
    #  @current_order.user_id = user_id
    #  @current_order.retailer_id = retailer_id
    #  before_save_new_order
    #  @current_order.save!
    #  after_save_new_order
    #end


    session[:order_id] = @current_order ? @current_order.id : nil
    @current_order
  end

end

