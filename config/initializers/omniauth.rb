Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, '176544045776839', '6e0592075049b4b2df54607c09f25e67',
           :scope => 'email, user_birthday, publish_stream'
end



OmniAuth.config do |config|

  config.on_failure = Proc.new { |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }
  config.full_host = request.host

end