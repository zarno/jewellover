Jewellover::Application.routes.draw do

  get "lists_controller/index"

  resources :wallets
  resources :authentications

  # -- Note - Check 'Admin Static Pages' if route not in here   ###


  ###================= DEVISE SETUP ====================###
  devise_for :retailer, :controllers => {:registrations => "retailer/registrations"}, :skip => [:sessions, :password] do
    get "retailer/signup" => "retailer/registrations#new"
  end

  # ======================================================= ###

  match '/auth/:provider/callback', :to => 'authentications#create'
  match '/auth/failure', :to => 'authentications#failure'


  # === PAYPAL === #
  match "/notify_url" => "paypal_adaptive_payment_callbacks#notify", :as => :paypal_notify
  resources :paypal_adaptive_payment_callbacks

  # ======================================================= ####

  resources :bids
  resource :user_preferences
  resources :stores do
    resources :products, :only => [:index, :show]
  end

  resource :contact do
    post :store
  end

  # === COMMON ==== #
  # All COMMON resources/namespaces/scopes to be manually set
  # to allow for /:store_name routing at latest
  match "/" => "products#homepage"
  match "/products" => "products#index"
  match "/categories" => "products#categories"
  match "/upcoming" => "products#upcoming"
  match "/recent" => "products#latest"

  match "/testimonials" => "posts#testimonials"
  match "/whats_new" => "posts#whats_new"
  #match "/orders" => "orders#index", :as => :orders
  #match '/dashboard' => 'dashboard#index', :as => :dashboard
  #match '/watchlist' => 'watchlists#index', :as => :watchlist

  # === NICE Member URLS ==== #
  match "/my_dashboard" => "dashboard#index", :as => :my_dashboard
  match "/my_orders" => "orders#index", :as => :my_orders
  match "/my_offers" => "bids#index", :as => :my_offers
  match "/my_watchlist" => "watchlists#index", :as => :my_watchlist
  match "/my_account" => "users#edit", :as => :my_account
  match "/my_wallet" => "wallets#show", :as => :my_wallet


  # === AJAX Routes === #
  match "/server_time" => "spree/base#server_time"
  match "/site_product_info" => "spree/base#site_product_info"
  match "/products/view_style" => "products#view_style"
  # match "/contact" => "static#contact", :as => :contact
  # match "/how_it_works" => "static#how_it_works"
  # match "/terms_and_conditions" => "static#terms_and_conditions"
  # match "/privacy_policy" => "static#privacy_policy"
  # match "/faqs" => "static#faqs"
  # match "/retailer_terms" => "static#retailer_terms"


  # ==== Google Split Test Experiments ====== #
  match "/v2" => "products#homepage"

  # =======   STORE NAME ROUTING... GLOBAL ========= #

  # match "/stores/:store_id/products/:id" => "products#store_product", :as => :store_product
  # match "/:store_name" => "stores#show", :as => :store_profile
  # match "/stores" => "stores#index", :as => :stores

  # match "/:store_name" => "stores#show", :as => :store
  # match "/:store_name/products" => "products#index", :as => :store_products

  #scope :module => :stores do
  #  resources :stores, :only => [:index, :show]
  #end


  # === FUNCTION === #
  match '/users/add_product_to_watchlist' => 'users#add_product_to_watchlist', :via => :post
  match '/users/remove_product_from_watchlist' => 'users#remove_product_from_watchlist', :via => :post

  # route globbing for pretty nested taxon and product paths
  match '/t/*id' => 'taxons#show', :as => :nested_taxons


  ###=================  API  ==========================###

  namespace :api do

    match "/taxonomies" => "products#taxonomies"

    resources :retailers, :only => [:show, :index] do
      resources :products, :only => [:show, :index, :create, :update] do
        match "/images/:image_id" => "products#delete_image", :via => :delete
      end
    end
    resources :products, :only => [:index, :show]

  end

#======================================================#


###===================== Admin ========================###
  namespace :admin do

    resources :lists do
      member do
        match "upload_csv" => "lists#upload_csv"
        get :download_csv
      end

      resources :list_recipients, :except => [:edit, :update, :show], :as => :contacts
    end
    resources :coupons
    resources :posts
    resources :users do
      post :toggle_ban, :on => :member
    end
    resources :reports, :only => [:index, :show] do
      collection do
        get :sales_total
        get :leveredge_total
      end
    end
    resources :stores do
      member do
        post :toggle_website
        post :toggle_facebook
      end
    end
    resources :products do
      member do
        post :product_campaign
        post :toggle_relist
        get :reset
      end
    end

  end

#======================================================#

###===================== Retailer =====================###

  namespace :retailer do


    match "/" => 'products#index'
    match "/support" => 'static#support', :as => :support


    resource :retailer_detail
    resource :settings


    resources :stores
    resources :zones
    resources :users
    resources :countries do
      resources :states
    end
    resources :states
    resources :promotions
    resources :tax_categories
    resources :configurations
    resources :products do
      member do
        get :clone
        get :finish
        get :start
        post :archive
        post :toggle_relist
        post :refund
      end
      resources :product_properties
      resources :images
      resources :variants
      resources :option_types do
        member do
          get :select
          get :remove
        end
        collection do
          get :available
          get :selected
        end
      end
      resources :taxons do
        member do
          get :select
          delete :remove
          get :finish
        end
        collection do
          post :available
          get :selected
        end
      end
    end
    resources :option_types
    resources :properties do
      collection do
        get :filtered
      end
    end
    resources :prototypes do
      member do
        get :select
      end
      collection do
        get :available
      end
    end
    resource :inventory_settings
    resources :google_analytics
    resources :orders do
      member do
        put :fire
        get :fire
        post :resend
        get :history
        get :user
      end
      resources :adjustments
      resources :line_items
      resources :shipments do
        member do
          put :fire
        end
      end
      resources :return_authorizations do
        member do
          put :fire
        end
      end
      resources :payments do
        member do
          put :fire
        end
      end
    end
    resource :general_settings
    resources :taxonomies do
      member do
        get :get_children
      end
      resources :taxons
    end
    resources :reports, :only => [:index, :show] do
      collection do
        get :sales_total
      end
    end
    resources :shipments
    resources :shipping_methods
    resources :shipping_categories
    resources :tax_rates
    resource :tax_settings
    resources :calculators
    resources :product_groups do
      resources :product_scopes
    end
    # resources :trackers
    # resources :payment_methods
    # resources :mail_methods
  end


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/show.html.
  # root :to => "welcome#show"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
