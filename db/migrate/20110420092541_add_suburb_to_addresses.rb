class AddSuburbToAddresses < ActiveRecord::Migration
  def self.up
    add_column :addresses, :suburb, :string
  end

  def self.down
    remove_column :addresses, :suburb
  end
end
