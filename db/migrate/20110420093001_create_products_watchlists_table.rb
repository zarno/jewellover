class CreateProductsWatchlistsTable < ActiveRecord::Migration
  def self.up
    create_table :products_watchlists, :id => false do |t|

      t.integer :product_id
      t.integer :watchlist_id

    end
  end

  def self.down
    drop_table :products_watchlists
  end
end
