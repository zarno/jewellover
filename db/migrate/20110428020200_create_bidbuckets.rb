class CreateBidbuckets < ActiveRecord::Migration
  def self.up
    create_table :bidbuckets do |t|
      t.integer :product_id

      t.timestamps
    end
  end

  def self.down
    drop_table :bidbuckets
  end
end
