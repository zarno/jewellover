class AddStateAndCloseTimeToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :state, :string
    add_column :products, :closetime, :datetime
    add_column :products, :interval, :integer
  end

  def self.down
    remove_column :products, :closetime
    remove_column :products, :state
    remove_column :products, :interval
  end
end
