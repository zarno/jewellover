class AddRetailerIdToOrder < ActiveRecord::Migration
  def self.up
    add_column :orders, :retailer_id, :integer
  end

  def self.down
    remove_column :orders, :retailer_id
  end
end
