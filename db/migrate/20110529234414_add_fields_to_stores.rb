class AddFieldsToStores < ActiveRecord::Migration
  def self.up
    add_column :stores, :logo_file_name, :string
    add_column :stores, :logo_content_type, :string
    add_column :stores, :logo_file_size, :integer
    add_column :stores, :logo_updated_at, :datetime
    add_column :stores, :license, :string
    add_column :stores, :abn, :string
    add_column :stores, :description, :text
    add_column :stores, :address1, :string
    add_column :stores, :address2, :string
    add_column :stores, :suburb, :string
    add_column :stores, :state, :string
    add_column :stores, :city, :string
    add_column :stores, :postcode, :string
    add_column :stores, :country, :string
    add_column :stores, :fname, :string
    add_column :stores, :lname, :string
    add_column :stores, :phone, :string
    add_column :stores, :fax, :string
    add_column :stores, :website, :string
  end

  def self.down
    remove_column :stores, :website
    remove_column :stores, :fax
    remove_column :stores, :phone
    remove_column :stores, :lname
    remove_column :stores, :fname
    remove_column :stores, :country
    remove_column :stores, :postcode
    remove_column :stores, :city
    remove_column :stores, :suburb
    remove_column :stores, :state, :string
    remove_column :stores, :address1
    remove_column :stores, :address2
    remove_column :stores, :description
    remove_column :stores, :license
    remove_column :stores, :abn, :string
    remove_column :stores, :logo_file_name
    remove_column :stores, :logo_content_type, :string
    remove_column :stores, :logo_file_size, :integer
    remove_column :stores, :logo_updated_at, :datetime
  end
end