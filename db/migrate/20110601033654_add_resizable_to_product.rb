class AddResizableToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :resizable, :integer
  end

  def self.down
    remove_column :products, :resizable
  end
end
