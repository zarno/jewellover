class AddCertifiedToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :certified, :integer
  end

  def self.down
    remove_column :products, :certified
  end
end
