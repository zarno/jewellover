class AddDeptsToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :departments, :string
  end

  def self.down
    remove_column :products, :departments
  end
end
