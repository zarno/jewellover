class AddSoldPriceToVariants < ActiveRecord::Migration
  def self.up
    add_column :variants, :sold_price, :decimal
  end

  def self.down
    remove_column :variants, :sold_price
  end
end
