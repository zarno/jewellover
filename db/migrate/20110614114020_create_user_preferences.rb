class CreateUserPreferences < ActiveRecord::Migration
  def self.up
    create_table :user_preferences do |t|
      t.integer :user_id
      t.string :address1
      t.string :address2
      t.string :suburb
      t.string :city
      t.integer :postcode
      t.string :state
      t.string :country
      t.string :phone
      t.string :fname
      t.string :lname
      t.datetime :dob
      t.string :gender
      t.string :occupation

      t.timestamps
    end
  end

  def self.down
    begin
      drop_table :user_preferences rescue true
    end
  end
end
