class AddLeveredgeTokenToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :leveredge_token, :string
  end

  def self.down
    remove_column :users, :leveredge_token
  end
end
