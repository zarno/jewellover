class RemoveDepartmentsFromProduct < ActiveRecord::Migration
  def self.up
    remove_column :products, :departments
  end

  def self.down
    add_column :products, :departments, :string
  end
end
