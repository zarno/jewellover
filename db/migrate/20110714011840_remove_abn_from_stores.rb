class RemoveAbnFromStores < ActiveRecord::Migration
  def self.up
    remove_column :stores, :abn
  end

  def self.down
    add_column :stores, :abn, :string
  end
end
