class CreateRetailerDetails < ActiveRecord::Migration
  def self.up
    create_table :retailer_details do |t|
      t.integer :retailer_id
      t.string :abn
      t.string :trading_name
      t.string :pp_username
      t.string :pp_password
      t.string :pp_signature


      t.timestamps
    end
  end

  def self.down
    drop_table :retailer_details
  end
end
