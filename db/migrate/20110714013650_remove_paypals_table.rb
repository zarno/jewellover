class RemovePaypalsTable < ActiveRecord::Migration
  def self.up
    drop_table :paypals
  end

  def self.down
    create_table :paypals do |t|
      t.integer :retailer_id
      t.string :login
      t.string :password
      t.string :signature

      t.timestamps
    end
  end
end
