class AddFacebookDetailsToRetailerDetail < ActiveRecord::Migration
  def self.up
    add_column :retailer_details, :facebook, :string
  end

  def self.down
    remove_column :retailer_details, :facebook
  end
end
