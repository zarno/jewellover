class AddUpViaLeveredgeToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :leveredge, :boolean, :default => false
  end

  def self.down
    remove_column :products, :leveredge
  end
end
