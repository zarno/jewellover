class AddSoldAtToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :sold_at, :datetime
  end

  def self.down
    remove_column :products, :sold_at
  end
end
