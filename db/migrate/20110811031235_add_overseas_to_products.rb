class AddOverseasToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :overseas, :boolean
  end

  def self.down
    remove_column :products, :overseas
  end
end
