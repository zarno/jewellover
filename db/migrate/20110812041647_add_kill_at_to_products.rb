class AddKillAtToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :kill_at, :datetime
  end

  def self.down
    remove_column :products, :kill_at
  end
end
