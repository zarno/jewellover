class RemovePpFieldsFromRetailerDetail < ActiveRecord::Migration
  def self.up
    remove_column :retailer_details, :pp_username
    remove_column :retailer_details, :pp_password
    remove_column :retailer_details, :pp_signature
  end

  def self.down
    add_column :retailer_details, :pp_signature, :string
    add_column :retailer_details, :pp_password, :string
    add_column :retailer_details, :pp_username, :string
  end
end
