class AddPpEmailToRetailerDetail < ActiveRecord::Migration
  def self.up
    add_column :retailer_details, :pp_email, :string
  end

  def self.down
    remove_column :retailer_details, :pp_email
  end
end
