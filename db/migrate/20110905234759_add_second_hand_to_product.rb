class AddSecondHandToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :second_hand, :boolean
  end

  def self.down
    remove_column :products, :second_hand
  end
end
