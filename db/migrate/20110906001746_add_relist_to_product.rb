class AddRelistToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :relist, :boolean
  end

  def self.down
    remove_column :products, :relist
  end
end
