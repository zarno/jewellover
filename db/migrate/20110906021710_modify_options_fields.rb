class ModifyOptionsFields < ActiveRecord::Migration
  def self.up
    remove_column :products, :resizable
    remove_column :products, :certified
  end

  def self.down
    add_column :products, :resizable, :integer
    add_column :products, :certified, :integer
  end
end
