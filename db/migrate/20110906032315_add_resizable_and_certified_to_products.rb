class AddResizableAndCertifiedToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :resizable, :boolean
    add_column :products, :certified, :boolean
  end

  def self.down
    remove_column :products, :certified
    remove_column :products, :resizable
  end
end
