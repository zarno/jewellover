class AddPermalinkToStores < ActiveRecord::Migration
  def self.up
    add_column :stores, :permalink, :string
  end

  def self.down
    remove_column :stores, :permalink
  end
end
