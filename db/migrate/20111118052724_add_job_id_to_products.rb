class AddJobIdToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :job_id, :integer
  end

  def self.down
    remove_column :products, :job_id
  end
end
