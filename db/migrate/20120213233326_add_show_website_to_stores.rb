class AddShowWebsiteToStores < ActiveRecord::Migration
  def self.up
    add_column :stores, :show_website, :boolean, :default => false, :null => false
  end

  def self.down
    remove_column :stores, :show_website
  end
end
