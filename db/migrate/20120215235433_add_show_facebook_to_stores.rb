class AddShowFacebookToStores < ActiveRecord::Migration
  def self.up
    add_column :stores, :show_facebook, :boolean, :default => false, :null => false;
  end

  def self.down
    remove_column :stores, :show_facebook
  end
end
