class AddRelistCountToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :relist_count, :integer, :default => 0, :null => false
  end

  def self.down
    remove_column :products, :relist_count
  end
end
