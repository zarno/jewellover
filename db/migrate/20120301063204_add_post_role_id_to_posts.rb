class AddPostRoleIdToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :post_role_id, :integer
  end

  def self.down
    remove_column :posts, :post_role_id
  end
end
