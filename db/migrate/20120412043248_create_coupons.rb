class CreateCoupons < ActiveRecord::Migration
  def self.up
    create_table :coupons do |t|
      t.string :number, :unique => true, :limit => 20
      t.string :email
      t.integer :wallet_id
      t.integer :order_id
      t.decimal :value, :default => 20.00
      t.datetime :expires_at
      t.datetime :used_at

      t.timestamps
    end
  end

  def self.down
    drop_table :coupons
  end
end