class AddFeaturedToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :featured, :boolean, :default => false, :null => false
    add_column :posts, :published, :boolean, :default => true, :null => false
    add_column :posts, :author, :string
  end

  def self.down
    remove_column :posts, :published
    remove_column :posts, :featured
    remove_column :posts, :author
  end
end
