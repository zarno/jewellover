class AddLienItemsCounterCacherToVariants < ActiveRecord::Migration
  def self.up
    add_column :variants, :line_items_count, :integer, :default => 0

    Variant.reset_column_information
    Variant.all.each do |v|
      v.update_attributes :line_items_count => v.line_items.length
    end
  end

  def self.down
    remove_column :variants, :line_items_count
  end
end
