class AddBidCountToBidBucket < ActiveRecord::Migration
  def self.up
    add_column :bidbuckets, :bids_count, :integer, :default => 0

    Bidbucket.reset_column_information
    Bidbucket.all.each do |b|
      b.update_attributes :bids_count => b.bids.length
    end
  end

  def self.down
    remove_column :bidbuckets, :bids_count
  end
end
