class AddCounterCacheToStore < ActiveRecord::Migration
  def self.up
    add_column :stores, :products_count, :integer, :default => 0

    Store.reset_column_information
    Store.all.each do |s|
      s.update_attributes :products_count => s.products.length
    end
  end

  def self.down
    remove_column :stores, :products_count
  end
end
