class AddMissingIndexes < ActiveRecord::Migration
  def self.up
    add_index :users, :ship_address_id
    add_index :users, :bill_address_id
    add_index :orders, :user_id
    add_index :orders, :retailer_id
    add_index :orders, :bill_address_id
    add_index :orders, :ship_address_id
    add_index :orders, :shipping_method_id
    add_index :products, :tax_category_id
    add_index :products, :shipping_category_id
    add_index :products, :retailer_id
    add_index :products, :store_id
    add_index :products_watchlists, [:watchlist_id, :product_id]
    add_index :products_watchlists, [:product_id, :watchlist_id]
    add_index :shipments, :order_id
    add_index :shipments, :shipping_method_id
    add_index :shipments, :address_id
    add_index :properties_prototypes, [:prototype_id, :property_id]
    add_index :properties_prototypes, [:property_id, :prototype_id]
    add_index :calculators, [:calculable_id, :calculable_type]
    add_index :shipping_methods, :zone_id
    add_index :bids, :bidbucket_id
    add_index :bids, :user_id
    add_index :user_preferences, :user_id
    add_index :stores, :retailer_id
    add_index :bidbuckets, :product_id
    add_index :watchlists, :user_id
    add_index :posts, :post_role_id
    add_index :retailer_details, :retailer_id
    add_index :payments, :order_id
    add_index :payments, [:source_id, :source_type]
    add_index :payments, :payment_method_id
  end

  def self.down
    remove_index :users, :ship_address_id
    remove_index :users, :bill_address_id
    remove_index :orders, :user_id
    remove_index :orders, :retailer_id
    remove_index :orders, :bill_address_id
    remove_index :orders, :ship_address_id
    remove_index :orders, :shipping_method_id
    remove_index :products, :tax_category_id
    remove_index :products, :shipping_category_id
    remove_index :products, :retailer_id
    remove_index :products, :store_id
    remove_index :products_watchlists, :column => [:watchlist_id, :product_id]
    remove_index :products_watchlists, :column => [:product_id, :watchlist_id]
    remove_index :shipments, :order_id
    remove_index :shipments, :shipping_method_id
    remove_index :shipments, :address_id
    remove_index :properties_prototypes, :column => [:prototype_id, :property_id]
    remove_index :properties_prototypes, :column => [:property_id, :prototype_id]
    remove_index :calculators, :column => [:calculable_id, :calculable_type]
    remove_index :shipping_methods, :zone_id
    remove_index :bids, :bidbucket_id
    remove_index :bids, :user_id
    remove_index :user_preferences, :user_id
    remove_index :stores, :retailer_id
    remove_index :bidbuckets, :product_id
    remove_index :watchlists, :user_id
    remove_index :posts, :post_role_id
    remove_index :retailer_details, :retailer_id
    remove_index :payments, :order_id
    remove_index :payments, :column => [:source_id, :source_type]
    remove_index :payments, :payment_method_id
  end
end
