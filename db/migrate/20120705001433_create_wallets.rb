class CreateWallets < ActiveRecord::Migration
  def self.up
    create_table :wallets do |t|
      t.integer :user_id, :null => false
      t.integer :credits, :default => 0, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :wallets
  end
end
