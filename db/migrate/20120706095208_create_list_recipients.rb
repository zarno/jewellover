class CreateListRecipients < ActiveRecord::Migration
  def self.up
    create_table :list_recipients do |t|
      t.integer :recipient_id
      t.integer :list_id
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :list_recipients
  end
end
