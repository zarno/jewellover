class CreateRecipients < ActiveRecord::Migration
  def self.up
    create_table :recipients do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :postcode
      t.string :gender
      t.datetime :birthday
      t.string :state
      t.string :provider

      t.timestamps
    end
  end

  def self.down
    drop_table :recipients
  end
end
