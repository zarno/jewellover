module SpreeSite

  class Engine < Rails::Engine


    def self.activate
      # Add your custom site logic here
      # Make Decorator inherit from base files
      Dir.glob(File.join(File.dirname(__FILE__), "../app/**/*_decorator*.rb")) do |c|
        Rails.env.production? ? require(c) : load(c)
      end


      if Spree::Config.instance
        Spree::Config.set(:auto_capture => true) # Paypal Express Auto Capture
        Spree::Config.set(:default_country_id => 12) # Default Country for deliveries and addresses
        Spree::Config.set(:logo => "images/common/logo.png")
        Spree::Config.set(:show_only_complete_orders_by_default => true)
        Spree::Config.set(:admin_products_per_page => 20)
      end

      Calculator::FreeShipping.register

      # Register the new abilities!! Ohh Yeh, Abilities...
      Ability.register_ability(RoleAbility)
    end

    def load_tasks
      # a workaround for the rake tasks being run twice as the engine loads from lib/ in spree and rails - Zarne
    end


    config.to_prepare &method(:activate).to_proc
  end


end

