namespace :db do

  desc "Erase and populate db"
  task :populate => :environment do
    require 'populator'

    [User, Product, Role].each(&:delete_all)

    %w(admin user retailer).each { |r| Role.create!(:name => r) }

    10.times do |user, n|
      password = "password"
      email = "#{user}@gcds.com.au"
      user = User.create!(
        :email => email,
        :password => password,
        :password_confirmation => password,
        :username => user)
      user.roles << Role.all.each.next
   end

  end

end
