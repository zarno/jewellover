namespace :jewellover do

  desc "Warn users of overdue orders - Heroku scheduler add-on"
  task :warn_users => :environment do
    puts "Warning users of late payemnts..."
    User.payment_warning
    puts "Users warned."
  end

  desc "Alert users of items on their watchlist - Heroku scheduler add-on"
  task :watchlist_alert => :environment do
    if Time.zone.now.thursday?
      puts "Alerting users of their watchlist..."
      User.watchlist_alert
      puts "Users alerted."
    end
  end

  desc "Alert users of coupons expiring - Heroku scheduler add-on"
  task :coupon_alert => :environment do
    puts "Alerting users of their coupons..."
    Coupon.alert_users
    puts "Users alerted."
  end


end
