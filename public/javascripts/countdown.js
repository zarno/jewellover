/*******************************************************************************
 Copyright (c) 20011. Gold Coast Design Studio (GCDS)
 email: zarne@gcds.com.au
 site: http://www.gcds.com.au
 built: 08/04/2011
 ******************************************************************************/

/*
 * Name:Countdown
 * Version: 1.0
 */

(function($) {

    var methods = {

        init : function(options) {


            return this.each(function() {

                //DOM Elements html
                var $this = $(this);
                var $details = $this.find('.product_details');


                //Read Vars
                var $refresh = $details.find(".product_show").html(); // Refresh page on price drop if product show :action
                var $dropCount = Number($details.find('.drop_count').html());
                var $maxDropCount = $details.find(".max_drop_count").html();
                var $drops = $details.find('.next_drop').html();
                var $startTime = $details.find('.start_time').html(); // a few days back for now --- replace with: new Date($this.find('.start_time').html());
                var $state = $details.find('.product_state').html();

                // -- Write Vars
                var $currentPrice = $this.find('.currentPrice');
                var $currentDiscount = $this.find('.current_discount');
                var $timer = $this.find('.time');
                var $debug = $('#debug');

                var now, drops, startTime, interval, startPrice, reservePrice, timer;//, timeSpent, currentPrice, reservePrice, valueDrop, discount;
                var showChange = true;

                //Item Vars
                startPrice = new Number($details.find('.start_price').html()).toFixed(2);
                reservePrice = new Number($details.find('.reserve_price').html());
                interval = new Number($details.find('.interval').html()) * 60 * 1000; //60,30,15mins in milliseconds!!

                drops = new Date($drops).getTime();
                startTime = new Date($startTime).getTime();

                /* =============== DEBUG! ==============*/
                //$debug.show();
                //$details.show();

                function debug(item) {
                    $debug.html(item + "<br/>");
                }


                /* =============== COUNTDOWN!! ============= */
                // Show info straight up not after 1 second!

                var k = 0;
                var time_diff = 0;

                var countdown = setInterval(updateCounter, 1000);

                function updateCounter() {

                    now = new Date(window.current_time).getTime();

                    // Timer countdown literally
                    timer = (drops - (now + time_diff)) / 100;
                    timer.toFixed();

                    // if product is not live yet, minus 1 interval to replicate count down to starting time
                    if (startTime > now) {
                        timer = timer - (interval / 100);
                        showChange = false;
                    }



                    $timer.html(format_output(timer));

                    if ($timer.hasClass("homepage")) {
                        $timer.parent().css("background-color", "#ffffff");
                    } else {
                        $timer.find(".time").css("color", "#ffffff");
                    }

//                    console.log(timer);
//                    console.log(time_diff);
//                    console.log(now);
//                    console.log($refresh);

                    // Whats happens at end of countdown ??
                    if (timer <= 0) {

                        // Dont show the price drop if product is sold or expired
                        if (showChange) {
                            drops += interval;
                            $dropCount++;
                            updateItem();
                        } else {
                            clearInterval(countdown);
                            location.reload(true);
                        }


                        if ($state == "upcoming") {
                            $this.find('.patch').hide();
                        }

                        if ($refresh == "true") {
                            clearInterval(countdown);
                            location.reload(true);
                        }


                    } else if (timer <= 100 && k == 1) {

                        //reset every 1 second
                        k = 0;

                        //Animate Clock in last 10 seconds
                        if ($timer.hasClass("homepage")) {
                            $timer.parent().css("background-color", '#ffffff');
                        } else {
                            $timer.find("span").css("color", "#ffaa00");
                        }

                    } else if (timer <= 100) {

                        k++;
                        //Animate Clock in last 10 seconds
                        if ($timer.hasClass("homepage")) {
                            $timer.parent().css("background-color", "#ffdada");
                        } else {
                            $timer.find(".time").css("color", "#ffffff");
                        }

                    }

                    time_diff += 1000;

                }

                updateItem();

                // ----------=   FUNCTIONS   =----------- /

                // Current Price based on time since started at 1% price drops !!
                function getCurrentPrice() {

                    // CP = SP - (DV * DC)
                    var dv = startPrice * 0.01; // 1% of start price - drop value
                    return Number(startPrice - (dv * $dropCount));

                }

                // Figures are double digits always
                function add_leading_zero(n) {
                    if (n.toString().length < 2) {
                        return '0' + n;
                    } else {
                        return n;
                    }
                }


                // Format the timer clock!! 2 hours, 10 mins = 02:00;
                function format_output(time) {


                    var hours, minutes, seconds;


                    if (isNaN(time) || time <= 0) {
                        return "Loading...";
                    }

                    seconds = Math.floor((time / 10) % 60);
                    minutes = Math.floor(time / 10 / 60) % 60;
                    hours = Math.floor(time / 10 / 60 / 60) % 24;

                    seconds = add_leading_zero(seconds);
                    minutes = add_leading_zero(minutes);
                    hours = add_leading_zero(hours);

                    seconds = '<span class="countdown">' + seconds + '</span>';

                    return   hours + ":" + minutes + ":" + seconds;

                }

                function updateItem() {


                    var currentPrice = getCurrentPrice();
                    var currentDiscount = 100 - (currentPrice / startPrice * 100);

                    // When price is updated check to see if reserve has been met ------- TODO check HIGHEST bid too
                    if (currentPrice < reservePrice) {

                        $currentPrice.html("Item Gone");
                        $currentDiscount.html($maxDropCount + "%");
                        $timer.html("00:00:00");

                        clearInterval(countdown);
                        location.reload();

                    } else {

                        $currentPrice.html("$" + currentPrice.toFixed(2));
                        $currentDiscount.html(currentDiscount.toFixed(0) + "%");

                    }

                }

                /*=========================================*/

            });

        }
    };


    $.fn.countdown = function(method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }

    };


})(jQuery);