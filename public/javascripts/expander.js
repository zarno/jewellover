/*******************************************************************************
 Copyright (c) 20011. Gold Coast Design Studio (GCDS)
 email: zarne@gcds.com.au
 site: http://www.gcds.com.au
 built: 08/04/2011
 ******************************************************************************/

/*
 * Name:Expander - Read More
 * Author: Zarne Dravitzki
 * Email: zarne@scratch22.net
 * Date: Mar 2012
 * Version: 1.0
 */

(function($) {

    var methods = {

        init : function(options) {

            return this.each(function() {

                $this = $(this);
                $text = $this.find('div');
                $trigger = $('<a href="javascript:void(0);" id="expander_trigger">Read More &raquo;</a>');
                $orig_height = $text.height();


                if ($orig_height > 36) {

                    $text.css({
                        'height':'36px',
                        'overflow':'hidden'
                    });
                    $this.append($trigger);

                    $('#expander_trigger').toggle(function() {
                        $text.animate({
                            'height': $orig_height
                        }, 500, function() {
                            $trigger.html("Minimise &laquo;");
                        });
                    }, function() {
                        $text.animate({
                            'height': '36px'
                        }, 500, function() {
                            $trigger.html("Read More &raquo;");
                        });
                    });

                }

            });

        }
    }


    $.fn.expander = function(method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }

    };


})
    (jQuery);