/*******************************************************************************
 Copyright (c) 20012. Scratch22
 email: zarne@scratch22.com.au
 site: http://www.scratch22.net
 built: 05/04/2012
 ******************************************************************************/

/*
 * Name:Fuse-Monitor
 * Version: 1.0
 */

(function($) {

    $.fn.fuseMonitor = function(options) {

        var settings = $.extend({
            product_info  :'.product_details',
            width         :500, // Integer Pixel width used for calculations!
            show_icons    :true // SHow the icons end price and start price
        }, options);

        return this.each(function() {

            var $this = $(this);
            var $total_width = settings.width; //settings.width;
            var $details = $(settings.product_info);

            // CUSTOM AUCTION VARIABLES
            // Enter Your custom variables you'll find in any auction...
            // You can use any data with this formula
            // You can customize the icons by loading them in the HTML with the ids..
            var $endPrice = parseFloat($(".reserve_price", $details).text()); //Dollars
            var $startPrice = parseFloat($(".start_price", $details).text());
            var $highestOffer = parseFloat($(".highest_bid", $details).text()) || $endPrice;
            var $currentPrice = parseFloat($("span.currentPrice", $details).text().split("$")[1]); // = parseFloat($details.on('load', '.current').split("$")[1]);
            var $status = $details.find(".product_state").text();

//            console.log($currentPrice);
//            console.log($("span.currentPrice", $details).html());


            // Find Length in pixels of Current Price Bar Indicator
            function findCurrentLengthInPixels(end, start, current, totalWidth) {
                // Make price relative to length
                current = (current - end);
                start = (start - end);

                var perc = (current / start) * 100;
                return Math.ceil((perc / 100) * totalWidth);
            }

            //Get Margin of current price bar from end price.
            function findHighestOfferMargin(end, start, highestOffer, totalWidth) {
                // Make price relative to length
                start = (start - end);
                if (highestOffer > end) {
                    highestOffer = highestOffer - end;
                } else {
                    highestOffer = 0;
                }

                var perc = (highestOffer / start) * 100;
                return Math.ceil((perc / 100) * totalWidth);
            }

            // Set Bar Param Var
            var offerMargin = findHighestOfferMargin($endPrice, $startPrice, $highestOffer, $total_width);
            var currentLength = findCurrentLengthInPixels($endPrice, $startPrice, $currentPrice, $total_width);


            //Create Bars


            $('<p/>', {
                id: 'base'
            }).appendTo($this).css("width", settings.width);

            $('<p/>', {
                id: 'current'
            }).appendTo('#base').css("width", settings.width);

            // Manipulate Bars
            $('#current').animate({
                    'margin-left': offerMargin,
                    'width': currentLength - offerMargin
                }, 1000,
                function() {
                    if (settings.show_icons) {
                        $(this).append($this.find("img").fadeIn());
                    }
                });


            $("<span class='fm_highest_offer'>Ends at <strong>$" + $highestOffer +
                "</strong></span><span class='fm_current_price'>Now <strong>$" + $currentPrice + "</strong></span>")
                .appendTo("#fuse_monitor");


            $(".fm_highest_offer").css('left', offerMargin); //- ($(this).innerWidth() / 2));
            $(".fm_current_price").css('left', currentLength);// - ($(this).innerWidth() / 2));


            //Show Data
            $this.hover(function() {
                $("span", $this).stop(true, true).fadeIn();
            }, function() {
                $("span", $this).stop(true, true).fadeOut();
            });

        });
    };
})(jQuery);