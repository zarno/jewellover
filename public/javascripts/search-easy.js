// Search within the context of which is shown or known.
// TODO Clean up, Build Gem extending meta-search and Build portable jquery plugin too
// http://stackoverflow.com/questions/1582534/calculating-text-width-with-jquery
// http://jsfiddle.net/Zarnos/Wg95s/ Fiddle with it

(function ($) {

    $.fn.showContext = function (c) {

        return this.each(function () {

            var $this = $(this); // Is the search text INPUT FIELD!
            var context_exists = c ? true : false; //Have we still got context?
            var context_span = $('<span id="search_context">' + c + '<a class="delete"></a></span>');
            //Append some containers to style up and insert data into
            var is_context = context_exists ? context_span.insertAfter($this) : false;

            $this.parent().append('<span id="org"></span>');
            $('#org').html($this.val());

            //Setup vars
            var contextWidth = $('#search_context').innerWidth();
            var start_width = $('#search_field').css('width').split('p')[0];
            var end_width = start_width - contextWidth;

            var width = $('#org').innerWidth();

            if(width > 0){
                $('#search_context').css({left:width+'px'});
            } else {
                $('#search_context').css({left:'40px'});
            }
            console.log(width);



            //On Key Up or change
            $this.bind("keyup change", function () {

                // If we dont have context we dont need to filter results
                if ($this.val() == '') {
                    $('#search_context').css({left:'40px'});
                } else if (context_exists) {
                    $('#org').html($this.val());
                    width = $('#org').innerWidth();

                    if (width > end_width) {
                        $('#search_context').css({
                            position:'relative',
                            left:'0px',
                            marginLeft:'0'
                        });

                        $('#search_field').css({
                            width:end_width + "px"
                        });
                    } else {
                        $('#search_context').css({
                            left:width + "px"
                        });
                    }
                }

            });

            $("#search_context a.delete").click(function () {
                $('#search_context').fadeOut(function () {
                    $this.parent().attr("action","/products");
                    $('#search_field').css({
                        width:start_width + "px"
                    });
                });
                context_exists = false;
            });
        });
    }

})(jQuery);