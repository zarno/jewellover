# Jewellover

[Jewellover](http://jewellover.heroku.com/)

## Git:

We have **two main remote** repos:

* **github**
* **heroku**

Plus **staging** and **production** (on heroku) for multi-environment configuration.

Therefore, when `pushing`, the remote repo must be specified as such:

`$ git push staging master`

Sample `.git/config` file

    [core]
      repositoryformatversion = 0
      filemode = true
      bare = false
    [branch "master"]
      remote = origin
      merge = refs/heads/master
    [remote "staging"]
      fetch = +refs/heads/*:refs/remotes/origin/*
      url = git@heroku.com:jewellover-staging.git
    [remote "github"]
      fetch = +refs/heads/*:refs/remotes/origin/*
      url = git@github.com:gc-designstudio/jewellover.git
    [remote "production"]
      fetch = +refs/heads/*:refs/remotes/origin/*
      url = git@heroku.com:jewellover.git

`git remote -v` should return something like this:

    [jewellover] $ git remote -v
    github	git@github.com:gc-designstudio/jewellover.git (fetch)
    github	git@github.com:gc-designstudio/jewellover.git (push)
    production	git@heroku.com:jewellover.git (fetch)
    production	git@heroku.com:jewellover.git (push)
    staging	git@heroku.com:jewellover-staging.git (fetch)
    staging	git@heroku.com:jewellover-staging.git (push)
    [jewellover] $

## Paypal Users
buyer@gcds.com.au
buyer_1317558127_per@gcds.com.au
317558071

buyer_1312210698_per@gcds.com.au
independent

## Heroku CLI:

Heroku command most include --app in order to identify the envirnment. Example:

`$ heroku rake db:migrate --app jewellover-staging`

## Common useful commands:
Importing the production DB in local ENV:

`$ heroku rake db:pull --app {APP_NAME}`

## Staging ENV:

Heroku app name: **jewellover-staging**

[http://jewellover-staging.heroku.com/](http://jewellover-staging.heroku.com/)

[Managing Multiple Environments for an App](http://devcenter.heroku.com/articles/multiple-environments)

### Staging configuration

With a production App:

`$ heroku addons`

It will list your custom addons plus the defaults ones... therefore, in the next command you only need to add the custom addons. (coma separated without spaces!)

`$ heroku create jewellover-staging --stack bamboo-mri-1.9.2 --remote staging --addons custom_domains:basic,memcache:5mb,newrelic:bronze,sendgrid:free,zerigo_dns:basic`

`$ heroku config:add RACK_ENV=staging --app jewellover-staging`

`$ git push staging master`

`$ cp config/environments/development.rb config/environments/staging.rb`

`$ cp db/development.sqlite3 db/staging.sqlite3`

Update the proper settings ,then:

`$ git push staging master`

`$ heroku rake db:migrate --app jewellover-staging`

`$ heroku db:push --app jewellover-staging`

## DB Backups

See `RAILS_ROOT/lib/task/cron.rake`

It's based on a Heroku cron job which will be called once a day. It requires at least:

    heroku addons:add pgbackups:basic
    heroku addons:add cron:daily

## Memcached

    heroku addons:add memcache

In your Gemfile:

    gem 'dalli'

In config/environments/production.rb:

    config.cache_store = :dalli_store

[Memcache heroku article](http://devcenter.heroku.com/articles/memcache)

## RVMRC

rvm use ruby-1.9.2-p290@jewellover

## Legal

[Gold Coast Web Design by Gold Coast Design Studio](http://www.gcds.com.au/)
