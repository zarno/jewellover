# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :coupon do
    number "MyString"
    user_id 1
    order_id 1
    value 1
    expiry "2012-04-12 14:32:48"
  end
end
