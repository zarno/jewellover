# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :identity do
    title "MyString"
    first_name "MyString"
    last_name "MyString"
    gender "MyString"
    birthdate "2012-07-06 19:25:31"
    marital_status "MyString"
    address1 "MyString"
    address2 "MyString"
  end
end
