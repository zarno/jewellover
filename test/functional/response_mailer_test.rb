require 'test_helper'

class ResponseMailerTest < ActionMailer::TestCase
  test "recipient_response" do
    mail = ResponseMailer.recipient_response
    assert_equal "Recipient response", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
