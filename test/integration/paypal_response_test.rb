require '../test_helper'

require 'minitest/autorun'

class PaypalNotificationTest < MiniTest::Unit::TestCase

  def test_response
    p paypal_response['transaction']['0']['.invoiceId']
  end

  private

  def paypal_response
    {"transaction"=>{
        "0"=>{
          ".is_primary_receiver"=>"true",
          ".id_for_sender_txn"=>"21U696918R561262L",
          ".receiver"=>"tony_1314838793_biz@jewellerymarketingsolutions.com",
          ".amount"=>"USD 129.95",
          ".invoiceId"=>"R423025426",
          ".status"=>"Completed",
          ".id"=>"7DH16329KA864600D",
          ".status_for_sender_txn"=>"Completed",
          ".paymentType"=>"SERVICE",
          ".pending_reason"=>"NONE"
        },
        "1"=>{".paymentType"=>"SERVICE",
          ".id_for_sender_txn"=>"3G055529P0825922L",
          ".is_primary_receiver"=>"false",
          ".status_for_sender_txn"=>"Completed",
          ".receiver"=>"tony_1310518490_biz@jewellerymarketingsolutions.com",
          ".amount"=>"USD 13.00",
          ".pending_reason"=>"NONE",
          ".id"=>"0VM9950879183652M",
          ".invoiceId"=>"R423025426",
          ".status"=>"Completed"
        }
      },
      "log_default_shipping_address_in_transaction"=>"false",
      "action_type"=>"PAY",
      "ipn_notification_url"=>"https://jewellover-staging.heroku.com//notify_url",
      "charset"=>"windows-1252",
      "transaction_type"=>"Adaptive Payment PAY",
      "notify_version"=>"UNVERSIONED",
      "cancel_url"=>"http://jewellover-staging.heroku.com/orders/R423025426/edit",
      "verify_sign"=>"AFcWxV21C7fd0v3bYYYRCpSSRl31A3m2pTT3pSTjjy9HegIFzz2Gqs8v",
      "sender_email"=>"buyer_1317558127_per@gcds.com.au",
      "fees_payer"=>"PRIMARYRECEIVER",
      "return_url"=>"http://jewellover-staging.heroku.com/checkout/paypal_adaptive_payment_finish",
      "memo"=>"Goods from Jewellover Staging",
      "reverse_all_parallel_payments_on_error"=>"false",
      "pay_key"=>"AP-5S664027HY4327447",
      "status"=>"COMPLETED",
      "test_ipn"=>"1",
      "payment_request_date"=>"Mon Oct 03 05:43:08 PDT 2011",
      "controller"=>"paypal_adaptive_payment_callbacks",
      "action"=>"notify"
    }
  end

  def paypal_raw_response
    'transaction%5B0%5D.is_primary_receiver=true&transaction%5B0%5D.id_for_sender_txn=21U696918R561262L&log_default_shipping_address_in_transaction=false&transaction%5B0%5D.receiver=tony_1314838793_biz%40jewellerymarketingsolutions.com&action_type=PAY&ipn_notification_url=https%3A//jewellover-staging.heroku.com//notify_url&transaction%5B1%5D.paymentType=SERVICE&transaction%5B0%5D.amount=USD+129.95&charset=windows-1252&transaction_type=Adaptive+Payment+PAY&transaction%5B1%5D.id_for_sender_txn=3G055529P0825922L&transaction%5B0%5D.invoiceId=R423025426&transaction%5B1%5D.is_primary_receiver=false&transaction%5B0%5D.status=Completed&notify_version=UNVERSIONED&transaction%5B0%5D.id=7DH16329KA864600D&cancel_url=http%3A//jewellover-staging.heroku.com/orders/R423025426/edit&transaction%5B1%5D.status_for_sender_txn=Completed&transaction%5B1%5D.receiver=tony_1310518490_biz%40jewellerymarketingsolutions.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31A3m2pTT3pSTjjy9HegIFzz2Gqs8v&sender_email=buyer_1317558127_per%40gcds.com.au&fees_payer=PRIMARYRECEIVER&transaction%5B0%5D.status_for_sender_txn=Completed&return_url=http%3A//jewellover-staging.heroku.com/checkout/paypal_adaptive_payment_finish&transaction%5B0%5D.paymentType=SERVICE&memo=Goods+from+Jewellover+Staging&transaction%5B1%5D.amount=USD+13.00&reverse_all_parallel_payments_on_error=false&transaction%5B1%5D.pending_reason=NONE&pay_key=AP-5S664027HY4327447&transaction%5B1%5D.id=0VM9950879183652M&transaction%5B0%5D.pending_reason=NONE&transaction%5B1%5D.invoiceId=R423025426&status=COMPLETED&transaction%5B1%5D.status=Completed&test_ipn=1&payment_request_date=Mon+Oct+03+05%3A43%3A08+PDT+2011'
  end

end
